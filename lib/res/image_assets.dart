class ImageAssetsSvg {
  static const String loading = 'assets/svg/loading.svg';
  static const String logo = 'assets/svg/logo.svg';
  static const String mcLogoSvg = 'assets/svg/mc_logo.svg';
  static const String close = 'assets/svg/close.svg';
  static const String back_arrow = 'assets/svg/back_arrow.svg';
  static const String handle = 'assets/svg/handle.svg';
  static const String anotherCatalog = 'assets/svg/another_catalog.svg';
  static const String home = 'assets/svg/home.svg';
  static const String logout = 'assets/svg/logout.svg';
  static const String settings = 'assets/svg/settings.svg';
  static const String switchSvg = 'assets/svg/switch.svg';
}
class ImageAssetsPng {
  static const String mcLogoPng = 'assets/png/mc_logo.png';
  static const String splashScreen = 'assets/png/SplashScreen.png';
  static const String onBoardingOne = 'assets/png/on_boarding_1.png';
  static const String onBoardingTwo = 'assets/png/on_boarding_2.png';
  static const String onBoardingThree = 'assets/png/on_boarding_3.png';
  static const String onBoardingFour = 'assets/png/on_boarding_4.png';
  static const String pot = 'assets/png/pot.png';
  static const String swipeTutorial = 'assets/png/swipe_tutorial.png';
  static const String manYellow = 'assets/png/man_yellow.png';
  static const String manYellowTwo = 'assets/png/man_yellowTwo.png';
  static const String googleLogo = 'assets/png/google_logo.png';
  static const String fridge = 'assets/png/fridge.png';
  static const String favoriteChef = 'assets/png/favorite_chef.png';
  static const String favoriteChefTwo = 'assets/png/favorite_chef_two.png';
  static const String favoriteChefThree = 'assets/png/favorite_chef_three.png';
  static const String efLogo = 'assets/png/ef_logo.png';
  static const String logo = 'assets/png/logo.png';
  static const String congratRight= 'assets/png/congrat_right.png';
  static const String congratLeft = 'assets/png/congrat_left.png';
  static const String appleLogo = 'assets/png/apple_logo.png';
}
class GifAssets{
  static const String loadingGif = 'assets/loading.gif';
}
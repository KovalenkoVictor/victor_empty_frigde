import 'package:flutter/cupertino.dart';
import 'package:google_fonts/google_fonts.dart';


class FontStyles {
  static TextStyle get textBold {
    return GoogleFonts.roboto(
      fontWeight: FontWeight.w700,
    );
  }
  static TextStyle get textMedium {
    return GoogleFonts.roboto(
      fontWeight: FontWeight.w500,
    );
  }
  static TextStyle get textRegular {
    return GoogleFonts.roboto(
      fontWeight: FontWeight.w400,
    );
  }
  static TextStyle get textLight {
    return GoogleFonts.roboto(
      fontWeight: FontWeight.w300,
    );
  }

  static TextStyle get interSemiBold {
    return GoogleFonts.inter(
      fontWeight: FontWeight.w600,
    );
  }


}
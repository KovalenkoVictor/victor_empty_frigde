class ConstRout {
  static const String onBoarding = 'On Boarding';
  static const String home = 'Home';
  static const String favorites = 'Favorites';
  static const String settings = 'Settings';
  static const String recipes = 'Recipes';
  static const String notification = 'Notification';
  static const String signInSignUp = 'Sign In Sign up';
  static const String detailCardsPage = 'Detail Cards Page';
}

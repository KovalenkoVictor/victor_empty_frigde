import 'package:flutter/material.dart';

class AppColors {
  static const Color wheat = Color(0xFFffde81);
  static const Color marigold = Color(0xFFffbd00);
  static const Color ocre = Color(0xFFc79300);
  static const Color pastelRed = Color(0xFFeb5757);
  static const Color black6 = Color(0xFF060606);
  static const Color blackTwo = Color(0xFF000000);
  static const Color transparent = Colors.transparent;
  static const Color white = Color(0xFFFFFFFF);
  static const Color whiteTwo = Color(0xFFe8e8e8);
  static const Color whiteThree = Color(0xFFf6f6f6);
  static const Color pinkishGrey = Color(0xFFbdbdbd);

  static const Color red = Color(0xFFff0000);




  static const Color kBlack = Color(0xFF16191D);
  static const Color kGreyTwo = Color(0xFF96A7AF);
  static const Color kGrey = Color(0xFF494949);
  static const Color kDarkGrey = Color(0xFF3C3C43);
  static const Color kLightGrey = Color(0xFF828282);
  static const Color kDarkWhite = Color(0xFFBDBDBD);
  static const Color kGreen = Color(0xFF5DB075);
  static const Color kRed = Color(0xFFE74C3C);
}

class Locales {
  static const String base = en;

  static const String en = 'en';
  static const String ru = 'ru';
  static const String he = 'he'; // Hebrew

  static const String ps = 'ps'; // Pashto
  static const String ur = 'ur'; // Urdu
  static const String ar = 'ar'; // Arabic
  static const String fa = 'fa'; // Farsi
}
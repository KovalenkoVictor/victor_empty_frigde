import 'package:base_project_template/res/image_assets.dart';
import 'package:flutter/material.dart';

class Loader extends StatelessWidget {
  double width;
  double height;

  Loader({
    required this.width,
    required this.height,
});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height:height,
      child:Image.asset(GifAssets.loadingGif),
    );
  }
}

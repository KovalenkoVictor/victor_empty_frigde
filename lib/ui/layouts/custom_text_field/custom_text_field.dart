import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  bool? textFieldInContainer;
  bool? textFieldLoaderAndShow;
  bool? iconSearch;
  Widget? endTextField;
  String? hintTextField;
  Widget? widget;
  TextEditingController? controller;
  Color? color;
  Function(String text)? validator;
  bool obscureText;
  Function(String text)? onChanged;
 EdgeInsetsGeometry margin;

  CustomTextField({
    required this.obscureText,
    this.textFieldLoaderAndShow,
    this.iconSearch,
    this.endTextField,
    this.hintTextField,
    this.textFieldInContainer,
    this.widget,
    this.controller,
    this.color,
    this.validator,
    this.onChanged,
    this.margin= const EdgeInsets.only(left: 16.0, right: 16.0, top: 8.0),
  });

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return Container(
      padding: textFieldInContainer! ? EdgeInsets.only(left: 8.0,) : EdgeInsets.all(0.0),
      margin: margin,
      height: 42.0,
      decoration: BoxDecoration(
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black.withOpacity(0.8),
            blurRadius: 3.0,
            offset: Offset(0, 2),
          ),
        ],
        color: AppColors.white,
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: textFieldInContainer!
          ? Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  alignment: Alignment.center,
                  width: textFieldLoaderAndShow! ? mediaQuery.width * 0.66: mediaQuery.width * 0.86,
                  child: TextFormField(
                    onChanged:onChanged,
                    obscureText: obscureText,
                    validator: validator as String? Function(String?)?,
                    controller: controller,
                    cursorColor: AppColors.marigold,
                    style: FontStyles.textRegular.copyWith(fontSize: 16.0, color: AppColors.blackTwo),
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        contentPadding: EdgeInsets.symmetric(vertical: 13.0, horizontal: -12.0),
                        hintStyle: FontStyles.textRegular.copyWith(fontSize: 16.0, color: color),
                        icon: !iconSearch!
                            ? SizedBox()
                            : Icon(
                                Icons.search_rounded,
                                color: AppColors.black6.withOpacity(0.36),
                                size: 22.0,
                              ),
                        hintText: hintTextField),
                  ),
                ),
                 Spacer(),
                !textFieldLoaderAndShow! ? SizedBox() : endTextField!,
              ],
            )
          : widget,
    );
  }
}
//Choose the food

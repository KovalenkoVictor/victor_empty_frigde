import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:flutter/material.dart';

class CustomIconInCard extends StatelessWidget {
  String text;
  IconData icon;
  double sizeIcon;
  Color color;

  CustomIconInCard({
    required this.text,
    required this.icon,
    required this.sizeIcon,
    this.color = AppColors.black6,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          icon,
          color: AppColors.marigold,
          size: sizeIcon,
        ),
        const SizedBox(width: 4.0),
        Text(
          text,
          style: FontStyles.textRegular.copyWith(
            fontSize: 12.0,
            color: color,
          ),
        ),
      ],
    );
  }
}

import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/image_assets.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/user_state/user_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_redux/flutter_redux.dart';

class RowMissIngredientsImage extends StatelessWidget {
  bool openAndCloseCard;
  int index;

  RowMissIngredientsImage({
    required this.openAndCloseCard,
    required this.index,
  });

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return Directionality(
      textDirection: FlutterDictionary.instance.isRTL ? TextDirection.rtl : TextDirection.ltr,
      child: StoreConnector<AppState, UserViewModel>(
        converter: UserViewModel.fromStore,
        builder: (context, vm) => Positioned(
          top: mediaQuery.height * 0.11,
          left:  FlutterDictionary.instance.isRTL ?mediaQuery.width * 0.17:mediaQuery.width * 0.34,
          child: Opacity(
            opacity: openAndCloseCard ? 1 : 0,
            child: Row(
              children: vm.missIngredients[index]
                  .take(4)
                  .map(
                    (e) => Container(
                      margin: EdgeInsets.only(right: 10.0),
                      width: 32.0,
                      height: 32.0,
                      child: Image.network(
                        e.image,
                        errorBuilder: (BuildContext? context, Object? exception, StackTrace? stackTrace) {
                          return Image.asset(
                            ImageAssetsPng.manYellowTwo,
                            fit: BoxFit.cover,
                          );
                        },
                      ),
                    ),
                  )
                  .toList(),
            ),
          ),
        ),
      ),
    );
  }
}

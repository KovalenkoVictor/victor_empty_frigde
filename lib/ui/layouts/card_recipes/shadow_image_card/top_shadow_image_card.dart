import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:flutter/material.dart';

class TopShadowImageCard extends StatelessWidget{
bool openAndCloseCard;
  TopShadowImageCard({
    required this.openAndCloseCard
});
  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return AnimatedPositioned(
      duration: Duration(seconds: 1),
      bottom: openAndCloseCard ? mediaQuery.height * 0.11 : mediaQuery.height * 0.256,
      child: AnimatedContainer(
        duration: Duration(seconds: 1),
        width: openAndCloseCard ? mediaQuery.width * 0.31 : mediaQuery.width - 32.0,
        height:mediaQuery.height*0.11,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12.0),
          gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.center, colors: [
            AppColors.blackTwo.withOpacity(0.5),
            AppColors.blackTwo.withOpacity(0.24),
            AppColors.blackTwo.withOpacity(0),
          ]),
        ),
      ),
    );
  }
}
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:flutter/material.dart';

class BottomShadowImageCard extends StatelessWidget{

  bool openAndCloseCard;
  BottomShadowImageCard({
    required this.openAndCloseCard
  });

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return AnimatedPositioned(
      duration: Duration(seconds: 1),
      bottom: openAndCloseCard ? 0 : mediaQuery.height * 0.204,
      child: AnimatedContainer(
        duration: Duration(seconds: 1),
        width: openAndCloseCard ? mediaQuery.width * 0.31 : mediaQuery.width - 32.0,
        height:mediaQuery.height*0.11,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(openAndCloseCard ? 10.0 : 0),
          gradient: LinearGradient(begin: Alignment.center, end: Alignment.bottomCenter, colors: [
            AppColors.blackTwo.withOpacity(0),
            AppColors.blackTwo.withOpacity(0.36),
            AppColors.blackTwo.withOpacity(0.7),
          ]),
        ),
      ),
    );
  }
}
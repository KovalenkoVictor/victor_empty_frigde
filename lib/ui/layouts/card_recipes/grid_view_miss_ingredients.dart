import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/res/image_assets.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/user_state/user_vm.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class GridViewMissIngredients extends StatelessWidget {
  bool openAndCloseCard;
  int index;

  GridViewMissIngredients({
    required this.index,
    required this.openAndCloseCard,
  });

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return StoreConnector<AppState, UserViewModel>(
      converter: UserViewModel.fromStore,
      builder: (context, vm) => Positioned(
        bottom: 10.0,
        left: 20.0,
        child: AnimatedOpacity(
          duration: Duration(milliseconds: 1500),
          opacity: openAndCloseCard ? 0 : 1,
          child: SizedBox(
            width: openAndCloseCard ? 0.1 : 340.0,
            height: openAndCloseCard ? 0.1 : mediaQuery.height * 0.154,
            child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 8 / 2,
                  crossAxisSpacing: 10,
                ),
                itemCount: vm.missIngredients[index].length,
                itemBuilder: (context, indexIngredient) {
                  return !openAndCloseCard
                      ? Row(
                          children: [
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                SizedBox(
                                  width: 32.0,
                                  height: 32.0,
                                  child: Image.network(
                                    vm.missIngredients[index][indexIngredient].image,
                                    errorBuilder: (BuildContext? context, Object? exception, StackTrace? stackTrace) {
                                      return Image.asset(
                                        ImageAssetsPng.manYellowTwo,
                                        fit: BoxFit.cover,
                                      );
                                    },
                                    fit: BoxFit.scaleDown,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              width: mediaQuery.width * 0.29,
                              child: RichText(
                                text: TextSpan(children: [
                                  TextSpan(
                                      text: '${vm.missIngredients[index][indexIngredient].name}: ',
                                      style: FontStyles.textBold.copyWith(color: AppColors.black6, fontSize: 12.0)),
                                  TextSpan(
                                      text: ' ${vm.missIngredients[index][indexIngredient].count} ',
                                      style: FontStyles.textRegular.copyWith(color: AppColors.black6, fontSize: 12.0)),
                                  TextSpan(
                                      text: '${vm.missIngredients[index][indexIngredient].description}',
                                      style: FontStyles.textRegular.copyWith(color: AppColors.black6, fontSize: 12.0)),
                                ]),
                              ),
                            ),
                          ],
                        )
                      : const SizedBox();
                }),
          ),
        ),
      ),
    );
  }
}

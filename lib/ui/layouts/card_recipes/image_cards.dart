import 'package:base_project_template/res/image_assets.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/user_state/user_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_redux/flutter_redux.dart';

class ImageCards extends StatelessWidget {
  bool openAndCloseCard;
  String image;

  ImageCards({
    required this.openAndCloseCard,
    required this.image,
  });

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return ClipRRect(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12.0),
          topRight: Radius.circular(12.0),
          bottomLeft: Radius.circular(openAndCloseCard ? 12.0 : 0),
          bottomRight: Radius.circular(openAndCloseCard ? 12.0 : 0)),
      child: AnimatedContainer(
        duration: Duration(seconds: 1),
        width: openAndCloseCard ? mediaQuery.width * 0.31 : mediaQuery.width - 32.0,
        height: mediaQuery.height * 0.156,
        child: Image.network(
          image,
          fit: BoxFit.cover,
          errorBuilder: (BuildContext? context, Object? exception, StackTrace? stackTrace) {
            return Image.asset(
              ImageAssetsPng.manYellowTwo,
              fit: BoxFit.cover,
            );
          },
        ),
      ),
    );
  }
}

import 'dart:math';
import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/dictionary_classes/recipes_dictionary.dart';
import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/const_rout.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/res/icon.dart';
import 'package:base_project_template/services/network_service/dto/recipe_dto/recipe_dto.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/route_service/route_vm.dart';
import 'package:base_project_template/store/shared/user_state/user_vm.dart';
import 'package:base_project_template/ui/layouts/card_recipes/custom_icon_in_card.dart';
import 'package:base_project_template/ui/layouts/card_recipes/grid_view_miss_ingredients.dart';
import 'package:base_project_template/ui/layouts/card_recipes/image_cards.dart';
import 'package:base_project_template/ui/layouts/card_recipes/name_cards.dart';
import 'package:base_project_template/ui/layouts/card_recipes/row_miss_ingredients_image.dart';
import 'package:base_project_template/ui/layouts/card_recipes/row_open_cards_and_icon_min_cal_lvl.dart';
import 'package:base_project_template/ui/layouts/card_recipes/shadow_image_card/bottom_shadow_image_card.dart';
import 'package:base_project_template/ui/layouts/card_recipes/shadow_image_card/top_shadow_image_card.dart';
import 'package:base_project_template/ui/pages/detail_cards_page/detail_cards_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

// ignore: must_be_immutable
class CardRecipes extends StatefulWidget {
  final RecipeDto recipe;
  int index;

  bool favoritePageOrRecipe;
  bool detailCardCarouselSlider;
  bool buttonFavorites;

  // ignore: use_key_in_widget_constructors
  CardRecipes({
    required this.index,
    required this.favoritePageOrRecipe,
    required this.detailCardCarouselSlider,
    required this.buttonFavorites,
    required this.recipe,
  });

  @override
  _CardRecipesState createState() => _CardRecipesState();
}

class _CardRecipesState extends State<CardRecipes> {
  bool _openAndCloseCard = true;
  RecipesDictionary? languageTexRecipes = FlutterDictionary.instance.language?.recipesDictionary ?? en.recipesDictionary;

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return Directionality(
      textDirection: FlutterDictionary.instance.isRTL ? TextDirection.rtl : TextDirection.ltr,
      child: StoreConnector<AppState, UserViewModel>(
        converter: UserViewModel.fromStore,
        builder: (context, vm) => Material(
          color: AppColors.white,
          child: AnimatedContainer(
            duration: Duration(seconds: 1),
            margin: EdgeInsets.all(16.0),
            height: _openAndCloseCard ? mediaQuery.height * 0.156 : mediaQuery.height * 0.36,
            decoration: BoxDecoration(
              color: AppColors.white,
              borderRadius: BorderRadius.circular(12.0),
              boxShadow: [
                BoxShadow(
                  color: AppColors.blackTwo.withOpacity(0.25),
                  blurRadius: 10.0,
                  offset: Offset(0, 3),
                ),
              ],
            ),
            child: StoreConnector<AppState, DetailCardsViewModel>(
              converter: DetailCardsViewModel.fromStore,
              builder: (context, vmDetailCards) => ClipRRect(
                borderRadius: BorderRadius.circular(12.0),
                child: Material(
                  color: AppColors.transparent,
                  child: StoreConnector<AppState, RouteViewModel>(
                    converter: RouteViewModel.fromStore,
                    builder: (context, route) => InkWell(
                      highlightColor: AppColors.wheat,
                      splashColor: AppColors.wheat,
                      onTap: () {
                        vmDetailCards.value(widget.index);
                        route.push(ConstRout.detailCardsPage, widget.recipe);
                      },
                      child: Row(
                        children: [
                          Stack(
                            children: [
                              Container(
                                width: widget.detailCardCarouselSlider == false ? mediaQuery.width - 32.0 : mediaQuery.width - 82.0,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: widget.recipe.missIngredients > 0 &&
                                              widget.favoritePageOrRecipe == true &&
                                              widget.detailCardCarouselSlider == false
                                          ? AppColors.red
                                          : AppColors.transparent),
                                  borderRadius: BorderRadius.circular(12.0),
                                ),
                              ),
                              ImageCards(
                                openAndCloseCard: _openAndCloseCard,
                                image: widget.recipe.image,
                              ),
                              widget.recipe.missIngredients > 0 && !_openAndCloseCard && widget.favoritePageOrRecipe == true ||
                                      widget.detailCardCarouselSlider == true
                                  ? TopShadowImageCard(openAndCloseCard: _openAndCloseCard)
                                  : SizedBox(),
                              widget.recipe.missIngredients > 0 && widget.favoritePageOrRecipe == true || widget.detailCardCarouselSlider == true
                                  ? BottomShadowImageCard(openAndCloseCard: _openAndCloseCard)
                                  : SizedBox(),
                              NameCards(
                                name: widget.recipe.name,
                                openAndCloseCard: _openAndCloseCard,
                              ),
                              widget.favoritePageOrRecipe == true || widget.buttonFavorites == true
                                  ? SizedBox()
                                  : Positioned(
                                      top: 7.0,
                                      right: FlutterDictionary.instance.isRTL ? mediaQuery.width * 0.8 : 10.0,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(360),
                                        child: Material(
                                          color: AppColors.transparent,
                                          child: SizedBox(
                                            width: 40.0,
                                            height: 40.0,
                                            child: InkWell(
                                              onTap: () {
                                                vm.removeFavorite(vm.token, widget.recipe.i.toString());
                                              },
                                              child: Icon(
                                                Icons.favorite,
                                                size: 30.0,
                                                color: AppColors.pastelRed,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                              widget.recipe.missIngredients > 0 && widget.favoritePageOrRecipe == true && widget.detailCardCarouselSlider == false
                                  ? AnimatedPositioned(
                                      duration: Duration(seconds: 1),
                                      bottom: _openAndCloseCard ? mediaQuery.height * 0.055 : mediaQuery.height * 0.168,
                                      left: _openAndCloseCard ? mediaQuery.width * 0.34 : 20,
                                      child: Text(
                                        languageTexRecipes!.youDontHave,
                                        style: FontStyles.textBold.copyWith(fontSize: 12.0, color: AppColors.pastelRed),
                                      ),
                                    )
                                  : Positioned(
                                      left: mediaQuery.width * 0.35,
                                      bottom: mediaQuery.height * 0.052,
                                      child: CustomIconInCard(
                                        icon: RecipeIcons.caloories,
                                        text: '${widget.recipe.calories.toInt()} cal',
                                        sizeIcon: 20.0,
                                      ),
                                    ),
                              const SizedBox(height: 6.0),
                              widget.recipe.missIngredients > 0 && widget.favoritePageOrRecipe == true && widget.detailCardCarouselSlider == false
                                  ? RowMissIngredientsImage(
                                      index: widget.index,
                                      openAndCloseCard: _openAndCloseCard,
                                    )
                                  : Positioned(
                                      left: FlutterDictionary.instance.isRTL ? mediaQuery.width * 0.183 : mediaQuery.width * 0.35,
                                      bottom: mediaQuery.height * 0.016,
                                      child: Row(
                                        children: [
                                          CustomIconInCard(
                                            text: '${widget.recipe.time} min',
                                            icon: RecipeIcons.timeer,
                                            sizeIcon: 20.0,
                                          ),
                                          const SizedBox(width: 24.0),
                                          CustomIconInCard(
                                            text: widget.recipe.level,
                                            icon: RecipeIcons.bars,
                                            sizeIcon: 14.0,
                                          ),
                                        ],
                                      ),
                                    ),
                              widget.recipe.missIngredients > 0 && widget.favoritePageOrRecipe == true && widget.detailCardCarouselSlider == false
                                  ? AnimatedPositioned(
                                      duration: Duration(seconds: 1),
                                      right: FlutterDictionary.instance.isRTL ? mediaQuery.width * 0.82 : 20.0,
                                      bottom: _openAndCloseCard ? 7.0 : mediaQuery.height * 0.212,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(360),
                                        child: Material(
                                          color: AppColors.white,
                                          child: SizedBox(
                                            width: 28.0,
                                            height: 28.0,
                                            child: InkWell(
                                              highlightColor: AppColors.wheat,
                                              splashColor: AppColors.wheat,
                                              onTap: () {
                                                setState(() {
                                                  _openAndCloseCard = !_openAndCloseCard;
                                                });
                                              },
                                              child: Transform.rotate(
                                                angle: _openAndCloseCard ? 0 : pi,
                                                child: Icon(Icons.keyboard_arrow_down_sharp),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  : SizedBox(),
                              widget.recipe.missIngredients > 0 && widget.favoritePageOrRecipe == true && widget.detailCardCarouselSlider == false
                                  ? GridViewMissIngredients(
                                      index: widget.index,
                                      openAndCloseCard: _openAndCloseCard,
                                    )
                                  : SizedBox(),
                              RowOpenCardsAndIconMinCalLvl(
                                favoritePageOrRecipe: widget.favoritePageOrRecipe,
                                recipe: widget.recipe,
                                openAndCloseCard: _openAndCloseCard,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

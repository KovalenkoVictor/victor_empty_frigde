import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/user_state/user_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_redux/flutter_redux.dart';

class NameCards extends StatelessWidget {
  bool openAndCloseCard;
  String name;

  NameCards({
    required this.openAndCloseCard,
    required this.name,
  });

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return AnimatedPositioned(
      left: FlutterDictionary.instance.isRTL
          ? openAndCloseCard
              ? 15.0
              : mediaQuery.width * 0.09
          : openAndCloseCard
              ? mediaQuery.width * 0.34
              : 10.0,
      top: 10.0,
      duration: Duration(seconds: 1),
      child: SizedBox(
        width: openAndCloseCard ? mediaQuery.width * 0.5 : mediaQuery.width * 0.8,
        child: Text(
          name,
          maxLines: 2,
          style: FontStyles.textRegular.copyWith(
            color: openAndCloseCard ? AppColors.black6 : AppColors.white,
            fontSize: 20.0,
            shadows: [
              Shadow(
                blurRadius: 6.0,
                color: AppColors.blackTwo.withOpacity(0.3),
                offset: Offset(0.0, 3.0),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

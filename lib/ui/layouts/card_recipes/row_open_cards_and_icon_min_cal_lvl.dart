import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/icon.dart';
import 'package:base_project_template/services/network_service/dto/recipe_dto/recipe_dto.dart';
import 'package:base_project_template/ui/layouts/card_recipes/custom_icon_in_card.dart';
import 'package:flutter/cupertino.dart';

// ignore: must_be_immutable
class RowOpenCardsAndIconMinCalLvl extends StatelessWidget {
  final RecipeDto recipe;
  bool openAndCloseCard;
  bool favoritePageOrRecipe;

  // ignore: use_key_in_widget_constructors
  RowOpenCardsAndIconMinCalLvl({
    required this.openAndCloseCard,
    required this.favoritePageOrRecipe,
    required this.recipe,
  });

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return Positioned(
      top: mediaQuery.height * 0.12,
      left: FlutterDictionary.instance.isRTL ? 0 : 10.0,
      right: FlutterDictionary.instance.isRTL ? 10 : 0,
      child: Row(
        children: [
          favoritePageOrRecipe? CustomIconInCard(
            text: '${recipe.time} min',
            icon: RecipeIcons.timeer,
            sizeIcon: 20.0,
            color: AppColors.white,
          ):SizedBox(),
          const SizedBox(width: 18.0),
          openAndCloseCard
              ? SizedBox()
              : CustomIconInCard(
                  icon: RecipeIcons.caloories,
                  text: '${recipe.calories} cal',
                  sizeIcon: 20.0,
                  color: AppColors.white,
                ),
          const SizedBox(width: 18.0),
          openAndCloseCard
              ? SizedBox()
              : CustomIconInCard(
                  text:recipe.level,
                  icon: RecipeIcons.bars,
                  sizeIcon: 14.0,
                  color: AppColors.white,
                ),
        ],
      ),
    );
  }
}

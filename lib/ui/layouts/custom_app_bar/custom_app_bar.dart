import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/app_styles/app_gradient.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/route_service/route_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  @override
  final Size preferredSize;
  final String title;
  final String pop;

  CustomAppBar({
    required this.title,
    required this.pop,
  }) : preferredSize = Size.fromHeight(45.0);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 88,
      alignment: Alignment.bottomCenter,
      padding: EdgeInsetsDirectional.only(
        start: 16.0,
        bottom: 6.0,
      ),
      decoration: BoxDecoration(
        gradient: AppGradient.wheatMarigold,
      ),
      child: Row(
        children: [
          Material(
            color: AppColors.transparent,
            child: StoreConnector<AppState, RouteViewModel>(
              converter: RouteViewModel.fromStore,
              builder: (context, route) => InkWell(
                borderRadius: BorderRadius.circular(4.0),
                onTap: () {
                  route.pop();
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.arrow_back_ios,
                      size: 28.0,
                    ),
                    Transform.translate(
                      offset: FlutterDictionary.instance.isRTL ? Offset(8,0) :Offset(-8,0),
                      child: Text(
                        pop,
                        style: FontStyles.textRegular.copyWith(fontSize: 16.0, color: AppColors.black6),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Spacer(),
          Text(
            title,
            style: FontStyles.textBold.copyWith(fontSize: 24.0, color: AppColors.black6),
          ),
          Spacer(flex: 2),
        ],
      ),
    );
  }
}

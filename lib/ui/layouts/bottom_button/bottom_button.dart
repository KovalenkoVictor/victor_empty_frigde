import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BottomButton extends StatelessWidget {
  final String title;
  final void Function() onTap;
  final EdgeInsetsGeometry marginShadow;
  final EdgeInsetsGeometry margin;

  // ignore: use_key_in_widget_constructors
  const BottomButton({
    required this.title,
    required this.onTap,
    this.marginShadow = const EdgeInsets.symmetric(horizontal: 42.0),
    this.margin = const EdgeInsets.symmetric(horizontal: 32.0),
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColors.transparent,
      child: Stack(
        children: [
          Container(
            margin: marginShadow,
            height: 56.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(18.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: AppColors.ocre.withOpacity(0.3),
                  blurRadius: 10,
                  offset: Offset(0, 10),
                ),
              ],
            ),
          ),
          Container(
            margin: margin,
            height: 56.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12.0),
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  AppColors.wheat,
                  AppColors.marigold,
                ],
              ),
            ),
            child: Material(
              color: AppColors.transparent,
              child: InkWell(
                borderRadius: BorderRadius.circular(12.0),
                onTap: onTap,
                child: Center(
                  child: Text(
                    title,
                    style: FontStyles.textMedium.copyWith(color: AppColors.white, fontSize: 24.0),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

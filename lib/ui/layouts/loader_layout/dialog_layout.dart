import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/ui/layouts/loader_layout/dialog_layout_vm.dart';

class DialogLayout extends StatelessWidget {
  final Widget child;

  DialogLayout({
    required this.child,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, DialogLayoutVM>(
      converter: DialogLayoutVM.fromStore,
      builder: (BuildContext context, DialogLayoutVM vm) {
        return SizedBox(
          width: double.infinity,
          height: double.infinity,
          child: Stack(
            children: [
              child,
              ...vm.loaders.map<Widget>((loader) {
                return loader.widget;
              }).toList(),
            ],
          ),
        );
      },
    );
  }
}


import 'package:base_project_template/services/dialog_service/shared/i_loader.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:redux/redux.dart';

class DialogLayoutVM {
  final List<ILoader> loaders;

  DialogLayoutVM({
    required this.loaders,
  });

  static DialogLayoutVM fromStore(Store<AppState> store) {
    return DialogLayoutVM(loaders: []
     // loaders: store.state.loaderState.loaders,
    );
  }
}

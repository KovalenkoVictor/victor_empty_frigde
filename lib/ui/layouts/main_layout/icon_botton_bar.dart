import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/store/shared/route_service/route_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:simple_gradient_text/simple_gradient_text.dart';

class IconBottomBar extends StatelessWidget {
  String textIcon;
  IconData icon;
  double sizeIcon;
  bool marginIcon;
  Function onTap;
  String namePage;

  IconBottomBar({
    required this.textIcon,
    required this.icon,
    required this.sizeIcon,
    required this.marginIcon,
    required this.onTap,
    required this.namePage,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColors.transparent,
      child: InkWell(
        borderRadius: BorderRadius.circular(14.0),
        onTap: onTap as void Function()?,
        child: Container(
          width: 80.0,
          margin: EdgeInsets.only(top: marginIcon ? 4.0 : 0),
          child: Column(
            children: [
              ShaderMask(
                shaderCallback: (Rect bounds) {
                  return RadialGradient(
                    center: Alignment.topCenter,
                    radius: 1,
                    colors: <Color>[
                      RouteHelper.currentPage == namePage? AppColors.wheat : AppColors.black6.withOpacity(0.2),
                      RouteHelper.currentPage == namePage? AppColors.marigold : AppColors.black6.withOpacity(0.2),
                    ],
                  ).createShader(bounds);
                },
                child: Icon(
                  icon,
                  color: AppColors.white,
                  size: sizeIcon,
                ),
              ),
              GradientText(
                textIcon,
                style: FontStyles.textRegular.copyWith(
                  fontSize: 12.0,
                  //color: AppColors.black6.withOpacity(0.2),
                ),
                gradientDirection: GradientDirection.ttb,
                colors: [
                  RouteHelper.currentPage == namePage? AppColors.wheat : AppColors.black6.withOpacity(0.2),
                  RouteHelper.currentPage == namePage? AppColors.marigold : AppColors.black6.withOpacity(0.2),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

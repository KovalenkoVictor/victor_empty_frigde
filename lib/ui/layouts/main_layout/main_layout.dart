import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/dictionary_classes/bottom_navigation_bar_dictionary.dart';
import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/const_rout.dart';
import 'package:base_project_template/res/icon.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/route_service/route_helper.dart';
import 'package:base_project_template/store/shared/route_service/route_vm.dart';
import 'package:base_project_template/store/shared/user_state/user_vm.dart';
import 'package:base_project_template/ui/layouts/custom_app_bar/custom_app_bar.dart';
import 'package:base_project_template/ui/layouts/main_layout/icon_botton_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

// ignore: must_be_immutable
class MainLayout extends StatelessWidget {
  Widget body;
  String titleAppBar;
  String popAppBar;
  bool needAppBar;
  bool navigationButtonBar = true;

  // ignore: use_key_in_widget_constructors
  MainLayout({
    required this.body,
    required this.titleAppBar,
    required this.popAppBar,
    required this.needAppBar,
    required this.navigationButtonBar,
  });

  @override
  Widget build(BuildContext context) {
    final BottomNavigationBarDictionary? languageTextBottomNavigationBar =
        FlutterDictionary.instance.language?.bottomNavigationBarDictionary ?? en.bottomNavigationBarDictionary;
    return GestureDetector(
      onTapDown: (details) => FocusScope.of(context).requestFocus(FocusNode()),
      child: StoreConnector<AppState, RouteViewModel>(
        converter: RouteViewModel.fromStore,
        builder: (context, route) => StoreConnector<AppState, UserViewModel>(
          converter: UserViewModel.fromStore,
          builder: (context, vmUser) => WillPopScope(
            onWillPop: () async {
              if (RouteHelper.pages.isNotEmpty) {
                route.pop();
                return true;
              } else {
                return false;
              }
            },
            child: Scaffold(
              backgroundColor: AppColors.white,
              appBar: needAppBar
                  ? CustomAppBar(
                      title: titleAppBar,
                      pop: popAppBar,
                    )
                  : null,
              bottomNavigationBar: navigationButtonBar
                  ? Hero(
                      tag: '123',
                      child: Container(
                        color: AppColors.white,
                        height: 82.0,
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(bottom: 2.0),
                              height: 0.2,
                              color: AppColors.black6,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                IconBottomBar(
                                  namePage: ConstRout.home,
                                  icon: MyFlutterApp.vector__1_,
                                  marginIcon: true,
                                  sizeIcon: 30.0,
                                  textIcon: '${languageTextBottomNavigationBar!.home}',
                                  onTap: () {
                                    route.pushNamedAndRemoveUntil(ConstRout.home);
                                  },
                                ),
                                StoreConnector<AppState, UserViewModel>(
                                  converter: UserViewModel.fromStore,
                                  builder: (context, vmUser) => IconBottomBar(
                                    namePage: ConstRout.favorites,
                                    icon: Icons.favorite,
                                    marginIcon: false,
                                    sizeIcon: 34.0,
                                    textIcon: '${languageTextBottomNavigationBar.favorites}',
                                    onTap: () {
                                      vmUser.getListFavorites();
                                      route.push(ConstRout.favorites);
                                      Future.delayed(Duration(seconds: 1), () => vmUser.recipeOrFavoritesNeedValue(false));
                                    },
                                  ),
                                ),
                                IconBottomBar(
                                  namePage: ConstRout.settings,
                                  icon: MyFlutterApp.vector,
                                  marginIcon: false,
                                  sizeIcon: 30.0,
                                  textIcon: '${languageTextBottomNavigationBar.settings}',
                                  onTap: () {
                                    route.push(ConstRout.settings);
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                  : SizedBox(),
              body: body,
            ),
          ),
        ),
      ),
    );
  }
}

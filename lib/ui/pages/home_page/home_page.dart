import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/dictionary_classes/home_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/text_field_dictionary.dart';
import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/image_assets.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/route_service/route_vm.dart';
import 'package:base_project_template/store/shared/user_state/user_vm.dart';
import 'package:base_project_template/ui/layouts/bottom_button/bottom_button.dart';
import 'package:base_project_template/ui/layouts/custom_text_field/custom_text_field.dart';
import 'package:base_project_template/ui/layouts/loader/loader.dart';
import 'package:base_project_template/ui/layouts/main_layout/main_layout.dart';
import 'package:base_project_template/ui/pages/home_page/button_clear_all.dart';
import 'package:base_project_template/ui/pages/home_page/custom_clip_path.dart';
import 'package:base_project_template/ui/pages/home_page/ingredients_field_widget.dart';
import 'package:base_project_template/ui/pages/home_page/list_of_selected_ingredients.dart';
import 'package:base_project_template/utils/clean_behavior.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

// ignore: use_key_in_widget_constructors
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final TextEditingController _chooseFoodController = TextEditingController();
  TextFieldDictionary? languageTexField = FlutterDictionary.instance.language?.textFieldDictionary ?? en.textFieldDictionary;
  HomeDictionary? languageHomePage = FlutterDictionary.instance.language?.homeDictionary ?? en.homeDictionary;
  bool loaderIcon = false;
  List<String> ids = [];

  @override
  void dispose() {
    _chooseFoodController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return MainLayout(
      navigationButtonBar: true,
      titleAppBar: '_',
      popAppBar: '_',
      needAppBar: false,
      body: StoreConnector<AppState, UserViewModel>(
        converter: UserViewModel.fromStore,
        builder: (context, vm) => Container(
          color: AppColors.white,
          child: ScrollConfiguration(
            behavior: CleanBehavior(),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  ClipPath(
                    clipper: CustomClipPath(),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 118.0,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [AppColors.marigold, AppColors.wheat],
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 20.0),
                  CustomTextField(
                    obscureText: false,
                    color: AppColors.black6,
                    hintTextField: languageTexField!.chooseTheFood,
                    iconSearch: true,
                    textFieldLoaderAndShow: loaderIcon,
                    textFieldInContainer: true,
                    endTextField: loaderIcon
                        ? Container(
                            padding: EdgeInsets.only(right: 8.0),
                            child: Loader(width: 30.0, height: 30.0),
                          )
                        : SizedBox(),
                    controller: _chooseFoodController,
                    onChanged: (ingredientsText) {
                      vm.ingredientsToken(vm.token, _chooseFoodController.text);
                    },
                  ),
                  Stack(
                    children: [
                      vm.listAddedIngredients.isEmpty
                          ? Column(
                              children: [
                                const SizedBox(height: 37.0),
                                Container(
                                  padding: EdgeInsets.only(left: 50.0),
                                  alignment: Alignment.centerLeft,
                                  height: mediaQuery.height * 0.52,
                                  child: Image.asset(
                                    ImageAssetsPng.favoriteChefThree,
                                    fit: BoxFit.scaleDown,
                                  ),
                                ),
                              ],
                            )
                          : Column(
                              children: [
                                ButtonClearAll(),
                                ListOfSelectedIngredients(),
                              ],
                            ),
                      vm.listAddedIngredients.isEmpty
                          ? SizedBox()
                          : Positioned(
                              bottom: 35.0,
                              left: 0,
                              right: 0,
                              child: StoreConnector<AppState, RouteViewModel>(
                                converter: RouteViewModel.fromStore,
                                builder: (context, route) => BottomButton(
                                  title: languageHomePage!.watchRecipe,
                                  onTap: () {
                                    vm.getRecipe(vm.token,vm.ids);
                                  }),
                            ),
                      ),
                      _chooseFoodController.text.isNotEmpty
                          ? IngridientsFieldWidget(
                              callBack: () {
                                setState(() {
                                  _chooseFoodController.text = '';
                                });
                              },
                            )
                          : SizedBox(),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

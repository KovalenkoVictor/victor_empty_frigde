import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/res/image_assets.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/user_state/user_vm.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class IngridientsFieldWidget extends StatelessWidget {
  void Function() callBack;

  IngridientsFieldWidget({
    required this.callBack,
});
  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return StoreConnector<AppState, UserViewModel>(
      converter: UserViewModel.fromStore,
      builder: (context, vm) => Container(
        clipBehavior: Clip.antiAlias,
        margin: EdgeInsets.only(left: 16.0, right: 16.0, top: 12.0),
        height: vm.ingredients.length >= 3
            ? mediaQuery.height * 0.225
            : vm.ingredients.length == 2
                ? mediaQuery.height * 0.15
                : mediaQuery.height * 0.08,
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(12.0),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black.withOpacity(0.3),
              blurRadius: 3.0,
            ),
          ],
        ),
        child: MediaQuery.removePadding(
          removeTop: true,
          removeBottom: true,
          context: context,
          child: ListView.builder(
            itemCount: vm.ingredients.length,
            itemBuilder: (context, index) {
              return Material(
                color: AppColors.transparent,
                child: InkWell(
                  splashColor: AppColors.wheat,
                  highlightColor: Colors.transparent,
                  hoverColor: Colors.transparent,
                  onTap: () {
                    vm.addedIngredients([vm.ingredients[index]]);
                    vm.addIds(vm.ids..add(vm.ingredients[index].i.toString()));
                    callBack();
                  },
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 1.0),
                        width: double.infinity,
                        height: mediaQuery.height * 0.074,
                        child: Container(
                          padding: EdgeInsets.only(left: 22.0, top: 12.0, bottom: 12.0),
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: 10.0),
                                width: 60.0,
                                height: 60.0,
                                child: Image.network(
                                  vm.ingredients[index].image,
                                  errorBuilder: (BuildContext? context, Object? exception, StackTrace? stackTrace){
                                    return Image.asset(ImageAssetsPng.favoriteChef);
                                  },
                                ),
                              ),
                              Text(
                                vm.ingredients[index].name,
                                style: FontStyles.textRegular.copyWith(fontSize: 20.0, color: AppColors.black6.withOpacity(0.7)),
                              ),
                            ],
                          ),
                        ),
                      ),
                      vm.ingredients.length > 1
                          ? Container(
                              height: 1.0,
                              width: double.infinity,
                              color: AppColors.black6.withOpacity(0.5),
                            )
                          : SizedBox(),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

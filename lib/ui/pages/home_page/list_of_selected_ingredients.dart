import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/res/image_assets.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/user_state/user_vm.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

// ignore: must_be_immutable, use_key_in_widget_constructors
class ListOfSelectedIngredients extends StatelessWidget {
  int _value = -1;

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;

    return StoreConnector<AppState, UserViewModel>(
      converter: UserViewModel.fromStore,
      builder: (context, vm) => Container(
        padding: EdgeInsets.only(bottom: mediaQuery.height * 0.08),
        color: AppColors.transparent,
        height: mediaQuery.height * 0.61,
        child: MediaQuery.removePadding(
          removeTop: true,
          context: context,
          child: ListView.builder(
            itemCount: vm.listAddedIngredients.length,
            itemBuilder: (context, index) {
              _value++;
              return Dismissible(
                background: Container(
                  color: Colors.red,
                ),
                key: UniqueKey(),
                onDismissed: (DismissDirection direction) {
                  vm.removeIngredient(index);
                  index--;
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(18.0),
                    topLeft: Radius.circular(18.0),
                  ),
                  child: Container(
                    padding: EdgeInsets.only(
                      left: 10.0,
                      right: 10.0,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25.0),
                        topRight: Radius.circular(25.0),
                      ),
                    ),
                    height: 72.0,
                    child: Column(
                      children: [
                        Spacer(flex: 2),
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 4.0),
                              width: 62.0,
                              height: 48.0,
                              child: Image.network(
                                vm.listAddedIngredients[index][index - _value].image,
                                errorBuilder: (BuildContext? context, Object? exception, StackTrace? stackTrace) {
                                  return Image.asset(ImageAssetsPng.favoriteChef);
                                },
                              ),
                            ),
                            Text(
                              vm.listAddedIngredients[index][index - _value].name,
                              style: FontStyles.textRegular.copyWith(
                                color: AppColors.black6.withOpacity(0.7),
                                fontSize: 20.0,
                                shadows: [
                                  Shadow(
                                    blurRadius: 6.0,
                                    color: AppColors.blackTwo.withOpacity(0.3),
                                    offset: Offset(0.0, 2.0),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Spacer(),
                        Container(
                          height: 0.5,
                          width: double.infinity,
                          color: AppColors.black6.withOpacity(0.5),
                        )
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

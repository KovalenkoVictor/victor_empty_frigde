import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/dictionary_classes/home_dictionary.dart';
import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/user_state/user_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class ButtonClearAll extends StatelessWidget {
  HomeDictionary? languageHomePage = FlutterDictionary.instance.language?.homeDictionary ?? en.homeDictionary;
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, UserViewModel>(
      converter: UserViewModel.fromStore,
      builder: (context, vm) => Container(
        alignment: Alignment.centerRight,
        padding: EdgeInsets.only(right: 16.0, left: 16.0, top: 25.0, bottom: 4.0),
        child: Material(
          color: AppColors.transparent,
          child: InkWell(
            onTap: () {
              vm.clearIngredients();
            },
            child: Text(
              languageHomePage!.clearAll,
              style: FontStyles.textRegular.copyWith(color: AppColors.pastelRed, fontSize: 12.0),
            ),
          ),
        ),
      ),
    );
  }
}

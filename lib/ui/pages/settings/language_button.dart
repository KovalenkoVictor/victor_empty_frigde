import 'package:base_project_template/dictionary/flutter_delegate.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/language_state/language_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class LanguageButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Button(
          language: 'en',
          textButton: 'En',
        ),
        Button(
          language: 'ru',
          textButton: 'Ru',
        ),
        Button(
          language: 'he',
          textButton: 'He',
        ),
      ],
    );
  }
}

class Button extends StatelessWidget {
  final String language;
  final String textButton;

  Button({
    required this.language,
    required this.textButton,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 16.0, right: 8.0),
      child: Material(
        borderRadius: BorderRadius.circular(10.0),
        color:FlutterDictionaryDelegate.getCurrentLocale==language?AppColors.wheat.withOpacity(0.2):AppColors.white,
        child: StoreConnector<AppState, LanguageViewModel>(
          converter: LanguageViewModel.fromStore,
          builder: (context, vm) => InkWell(
            borderRadius: BorderRadius.circular(10.0),
            onTap: () {
              vm.needLanguage(language);
            },
            child: Container(
              width: 62.0,
              height: 40.0,
              alignment: Alignment.center,
              decoration: BoxDecoration(

                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(
                  color:FlutterDictionaryDelegate.getCurrentLocale==language?AppColors.marigold :AppColors.blackTwo.withOpacity(0.2),
                ),
              ),
              child: Text(
                textButton,
                style: FontStyles.textRegular.copyWith(fontSize: 16.0, color: AppColors.black6),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:flutter/material.dart';

class FieldLogOutAndNotification extends StatelessWidget {
  final String name;
  final IconData icon;
  final  void Function() onTap;

  FieldLogOutAndNotification({
    required this.name,
    required this.icon,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColors.white,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(18.0),
            topLeft: Radius.circular(18.0),
          ),
        ),
        height: 66.0,
        child: InkWell(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(18.0),
            topLeft: Radius.circular(18.0),
          ),
          onTap: onTap,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                name,
                style: FontStyles.textRegular.copyWith(
                  fontSize: 20.0,
                  color: AppColors.blackTwo,
                ),
              ),
              Icon(
                icon,
                color: AppColors.marigold,
                size: 28,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

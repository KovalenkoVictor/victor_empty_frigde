import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/dictionary_classes/app_bar_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/settings_dictionary.dart';
import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/const.dart';
import 'package:base_project_template/res/const_rout.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/res/image_assets.dart';
import 'package:base_project_template/services/dialog_service/dialog_service.dart';
import 'package:base_project_template/services/dialog_service/dialogs/exit_dialog/exit_dialog.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/route_service/route_vm.dart';
import 'package:base_project_template/ui/layouts/main_layout/main_layout.dart';
import 'package:base_project_template/ui/pages/settings/field_log_out_and_notification.dart';
import 'package:base_project_template/ui/pages/settings/language_button.dart';
import 'package:base_project_template/utils/clean_behavior.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class SettingsPage extends StatelessWidget {
  AppBarDictionary? languageTextAppBar = FlutterDictionary.instance.language?.appBarDictionary ?? en.appBarDictionary;
  SettingsDictionary? languageTex = FlutterDictionary.instance.language?.settingsDictionary ?? en.settingsDictionary;

  @override
  Widget build(BuildContext context) {
    return MainLayout(
        navigationButtonBar: true,
        body: StoreConnector<AppState, RouteViewModel>(
          converter: RouteViewModel.fromStore,
          builder: (context, route) => ScrollConfiguration(
            behavior: CleanBehavior(),
            child: SingleChildScrollView(
              child: Container(
                color: AppColors.white,
                padding: const EdgeInsets.all(14.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 54.0),
                    FieldLogOutAndNotification(
                      name: '${languageTex!.logOut}',
                      icon: Icons.logout,
                      onTap: () {
                        DialogService.instance.show(ExitDialog());
                      },
                    ),
                    Divider(height: 1, color: AppColors.blackTwo),
                    const SizedBox(height: 30.0),
                    Text(
                      '${languageTex!.language}',
                      style: FontStyles.textRegular.copyWith(fontSize: 20.0, color: AppColors.blackTwo),
                    ),
                    const SizedBox(height: 18.0),
                    LanguageButton(),
                    const SizedBox(height: 8.0),
                    FieldLogOutAndNotification(
                      name: '${languageTex!.notification}',
                      icon: Icons.arrow_forward_ios,
                      onTap: () {
                        route.push(ConstRout.notification);
                      },
                    ),
                    Divider(height: 1, color: AppColors.blackTwo),
                    Container(
                      padding: const EdgeInsets.only(top: 26.0, bottom: 10.0),
                      width: double.infinity,
                      child: Text(
                        '${languageTex!.aboutCompany}',
                        style: FontStyles.textRegular.copyWith(fontSize: 20.0, color: AppColors.blackTwo),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(bottom: 8.0),
                      height: 72.0,
                      width: double.infinity,
                      child: Image.asset(ImageAssetsPng.logo),
                    ),
                    Text(
                      AboutTitleText.text,
                      style: FontStyles.textRegular.copyWith(
                        fontSize: 16.0,
                        height: 1.6,
                        color: AppColors.black6.withOpacity(0.8),
                      ),
                    ),
                    const SizedBox(
                      width: double.infinity,
                      height: 20.0,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        titleAppBar: '${languageTextAppBar!.settings}',
        popAppBar: '${languageTextAppBar!.back}',
        needAppBar: true);
  }
}

import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/dictionary_classes/app_bar_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/recipes_dictionary.dart';
import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/res/image_assets.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/user_state/user_vm.dart';
import 'package:base_project_template/ui/layouts/card_recipes/card_recipes.dart';
import 'package:base_project_template/ui/layouts/main_layout/main_layout.dart';
import 'package:base_project_template/utils/clean_behavior.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

// ignore: must_be_immutable, use_key_in_widget_constructors
class RecipesPage extends StatelessWidget {
  AppBarDictionary? languageTextAppBar = FlutterDictionary.instance.language?.appBarDictionary ?? en.appBarDictionary;
  RecipesDictionary? languageTexRecipes = FlutterDictionary.instance.language?.recipesDictionary ?? en.recipesDictionary;

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return MainLayout(
      titleAppBar: languageTextAppBar!.recipes,
      popAppBar: languageTextAppBar!.back,
      needAppBar: true,
      navigationButtonBar: true,
      body: StoreConnector<AppState, UserViewModel>(
        converter: UserViewModel.fromStore,
        builder: (context, vm) => Container(
          color: AppColors.white,
          child: vm.recipe.isNotEmpty
              ? ScrollConfiguration(
                  behavior: CleanBehavior(),
                  child: ListView.builder(
                      itemCount: vm.recipe.length,
                      itemBuilder: (context, index){
                        return CardRecipes(
                          buttonFavorites: false,
                          detailCardCarouselSlider: false,
                          favoritePageOrRecipe:true,
                          index:index,
                          recipe:vm.recipe[index],
                        );
                      }),
                )
              : Column(
                  children: [
                    Spacer(),
                    Text(
                      languageTexRecipes!.yourListIsEmpty,
                      style: FontStyles.textBold.copyWith(fontSize: 24.0, color: AppColors.black6),
                    ),
                    Spacer(),
                    Container(
                      margin: EdgeInsets.only(left: 30.0),
                      height: mediaQuery.height * 0.5,
                      child: Image.asset(
                        ImageAssetsPng.favoriteChefTwo,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ],
                ),
        ),
      ),
    );
  }
}

import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:flutter/material.dart';
class OnBoardingCarouselItem extends StatelessWidget {
  final String titleText;
  final String image;
  final bool sizeContainerText;
  final int indexOnBoarding;

  // ignore: use_key_in_widget_constructors
  OnBoardingCarouselItem({
    required this.titleText,
    required this.image,
    required this.sizeContainerText,
    required this.indexOnBoarding,
  });

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return Stack(
      children: [
        Column(
          children: [
            Container(
              color: Colors.white,
              width: sizeContainerText ? 296 : 200,
              height: 56.0,
              child: Text(
                titleText,
                style: FontStyles.textBold.copyWith(fontSize: 24.0, color: AppColors.black6),
                textAlign: TextAlign.center,
              ),
            ),
            indexOnBoarding == 3
                ? Expanded(
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 44.0),
                      color: Colors.white,
                    ),
                  )
                : SizedBox(height: mediaQuery.height * 0.09),
            Container(
              color: Colors.white,
              padding: EdgeInsets.only(
                  left: indexOnBoarding == 2 ? 53.0 : 10.0, right: indexOnBoarding == 2 ? 0.0 : 10.0, top: indexOnBoarding == 2 ? 5.0 : 0),
              margin: EdgeInsets.only(
                left: indexOnBoarding == 3 ? 20.0 : 0.0,
                right: indexOnBoarding == 3 ? 20.0 : 0.0,
              ),
              width: indexOnBoarding == 0
                  ? double.infinity
                  : indexOnBoarding == 1
                      ? mediaQuery.width * 0.8
                      : indexOnBoarding == 2
                          ? mediaQuery.width * 1
                          : mediaQuery.width * 0.78,
              alignment: Alignment.topCenter,
              child: Image.asset(
                image,
                fit: indexOnBoarding == 0
                    ? BoxFit.fitWidth
                    : indexOnBoarding == 3
                        ? BoxFit.fitWidth
                        : BoxFit.cover,
                width: indexOnBoarding == 0
                    ? double.infinity
                    : indexOnBoarding == 1
                        ? mediaQuery.width * 0.8
                        : indexOnBoarding == 2
                            ? mediaQuery.width * 1
                            : mediaQuery.width * 0.78,
              ),
            )
          ],
        ),
      ],
    );
  }
}

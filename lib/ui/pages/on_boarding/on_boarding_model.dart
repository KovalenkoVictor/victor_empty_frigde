
class OnBoardingModel {
  final String titleText;
  final String image;
  final bool sizeContainerText;
  final int indexOnBoarding;

  OnBoardingModel({
    required this.titleText,
    required this.image,
    required this.sizeContainerText,
    required this.indexOnBoarding,
});
}
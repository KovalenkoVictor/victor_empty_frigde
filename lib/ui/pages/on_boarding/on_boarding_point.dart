
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:flutter/material.dart';

class OnBoardingPoint extends StatelessWidget {
  final bool isSelect;

  // ignore: use_key_in_widget_constructors
  OnBoardingPoint({
    required this.isSelect,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 4.5),
      width: 8.0,
      height: 8.0,
      decoration: BoxDecoration(
        color: isSelect ? AppColors.marigold : AppColors.transparent,
        shape: BoxShape.circle,
        border: Border.all(color: isSelect ? AppColors.transparent : AppColors.wheat, width: 1.5),
      ),
    );
  }
}
import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/dictionary_classes/on_boarding_dictionary.dart';
import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/const_rout.dart';
import 'package:base_project_template/res/image_assets.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/ui/layouts/bottom_button/bottom_button.dart';
import 'package:base_project_template/ui/pages/on_boarding/on_boarding_model.dart';
import 'package:base_project_template/store/shared/route_service/route_helper.dart';
import 'package:base_project_template/store/shared/route_service/route_vm.dart';
import 'package:base_project_template/ui/behavior/behavior.dart';
import 'package:base_project_template/ui/pages/on_boarding/on_boarding_carousel_item.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'on_boarding_point.dart';

// ignore: use_key_in_widget_constructors
class OnBoarding extends StatefulWidget {
  @override
  _OnBoardingState createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {
  final CarouselController carouselController = CarouselController();
  int indexOnBoardingShape = 0;
  int indexShape = 0;
  bool valueOpacityCongrat = true;

  @override
  Widget build(BuildContext context) {
    final OnBoardingDictionary languageTextOnBoarding = FlutterDictionary.instance.language?.onBoardingPage ?? en.onBoardingPage!;
    final List<OnBoardingModel> carouselItem = [
      OnBoardingModel(
        image: ImageAssetsPng.onBoardingOne,
        titleText: languageTextOnBoarding.titleTextOneOnBoarding,
        sizeContainerText: false,
        indexOnBoarding: 0,
      ),
      OnBoardingModel(
        image: ImageAssetsPng.onBoardingTwo,
        titleText: languageTextOnBoarding.titleTextTwoOnBoarding,
        sizeContainerText: true,
        indexOnBoarding: 1,
      ),
      OnBoardingModel(
        image: ImageAssetsPng.onBoardingThree,
        titleText: languageTextOnBoarding.titleTextThreeOnBoarding,
        sizeContainerText: true,
        indexOnBoarding: 2,
      ),
      OnBoardingModel(
        image: ImageAssetsPng.onBoardingFour,
        titleText: languageTextOnBoarding.titleTextFourOnBoarding,
        sizeContainerText: true,
        indexOnBoarding: 3,
      ),
    ];
    final Size mediaQuery = MediaQuery.of(context).size;
    return StoreConnector<AppState, RouteViewModel>(
      converter: RouteViewModel.fromStore,
      builder: (context, route) => WillPopScope(
        onWillPop: () async {
          if (RouteHelper.pages.first == ConstRout.onBoarding) {
            return false;
          } else {
            return false;
          }
        },
        child: Scaffold(
          backgroundColor:AppColors.white,
          body: Center(
            child: Stack(
              children: [
                SizedBox(
                  height: mediaQuery.height * 0.31,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      AnimatedOpacity(
                        duration: Duration(seconds: 1),
                        opacity: valueOpacityCongrat ? 0 : 1,
                        child: Image.asset(ImageAssetsPng.congratLeft),
                      ),
                      AnimatedOpacity(
                        duration: Duration(seconds: 1),
                        opacity: valueOpacityCongrat ? 0 : 1,
                        child: Image.asset(ImageAssetsPng.congratRight),
                      ),
                    ],
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: mediaQuery.height * 0.11),
                    ScrollConfiguration(
                      behavior: Behavior(),
                      child: CarouselSlider.builder(
                        carouselController: carouselController,
                        options: CarouselOptions(
                            viewportFraction: 1,
                            enableInfiniteScroll: false,
                            height: mediaQuery.height * 0.561,
                            onPageChanged: (int page, CarouselPageChangedReason _) {
                              indexShape = page;
                              if (indexShape == 3) {
                                valueOpacityCongrat = false;
                              } else {
                                valueOpacityCongrat = true;
                              }
                              setState(() {});
                            }),
                        itemCount: carouselItem.length,
                        itemBuilder: (BuildContext context, int itemIndex, int index) {
                          return OnBoardingCarouselItem(
                            image: carouselItem[itemIndex].image,
                            indexOnBoarding: carouselItem[itemIndex].indexOnBoarding,
                            sizeContainerText: carouselItem[itemIndex].sizeContainerText,
                            titleText: carouselItem[itemIndex].titleText,
                          );
                        },
                      ),
                    ),
                    Center(
                      child: SizedBox(
                        width: 120.0,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            for (int i = 0; i < carouselItem.length; i++)
                              OnBoardingPoint(
                                isSelect: i == indexShape,
                              ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 32.0),
                    BottomButton(
                      title: indexShape == 3 ? '${languageTextOnBoarding.buttonTextStart}' : '${languageTextOnBoarding.buttonTextNext}',
                      onTap: () {
                        if (indexShape == 3) {
                          route.pushNamedAndRemoveUntil(ConstRout.home);
                        }
                        carouselController.nextPage(duration: Duration(milliseconds: 300), curve: Curves.linear);
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

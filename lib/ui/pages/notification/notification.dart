import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/dictionary_classes/app_bar_dictionary.dart';
import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/ui/layouts/main_layout/main_layout.dart';
import 'package:base_project_template/utils/clean_behavior.dart';
import 'package:flutter/material.dart';

class NotificationPage extends StatelessWidget {
  AppBarDictionary? languageTextAppBar = FlutterDictionary.instance.language?.appBarDictionary ?? en.appBarDictionary;

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      navigationButtonBar: true,
        body: ScrollConfiguration(
          behavior: CleanBehavior(),
          child: SingleChildScrollView(
            child: Container(
              color: AppColors.white,
              padding: EdgeInsets.all(14.0),
              child: Column(
                children: [
                  const SizedBox(height: 66.0),
                  Material(
                    color: AppColors.transparent,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(18.0),
                          topLeft: Radius.circular(18.0),
                        ),
                      ),
                      child: InkWell(
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(18.0),
                          topLeft: Radius.circular(18.0),
                        ),
                        onTap: () {},
                        child: AnimatedContainer(
                          height: 72.0,
                          duration: Duration(seconds: 1),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Version',
                                style: FontStyles.textRegular.copyWith(fontSize: 20.0, color: AppColors.blackTwo),
                              ),
                              Icon(
                                Icons.keyboard_arrow_down_outlined,
                                color: AppColors.marigold,
                                size: 45,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Divider(
                    thickness: 1,
                    height: 0,
                  ),
                ],
              ),
            ),
          ),
        ),
        titleAppBar: '${languageTextAppBar!.notification}',
        popAppBar: '${languageTextAppBar!.back}',
        needAppBar: true);
  }
}

import 'dart:io';
import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/dictionary_classes/sign_in_sign_up_dictionary.dart';
import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/res/image_assets.dart';
import 'package:base_project_template/services/dialog_service/dialog_service.dart';
import 'package:base_project_template/services/dialog_service/dialogs/forgot_your_password/forgot_your_password_dialog.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/sign_in_sign_up_state/sign_in_sign_up_vm.dart';
import 'package:base_project_template/ui/layouts/bottom_button/bottom_button.dart';
import 'package:base_project_template/ui/layouts/custom_text_field/custom_text_field.dart';
import 'package:base_project_template/ui/layouts/main_layout/main_layout.dart';
import 'package:base_project_template/ui/pages/sign_in_sign_up/sign_in_register_google_and_apple_field.dart';
import 'package:base_project_template/ui/pages/sign_in_sign_up/sign_in_sign_up_button.dart';
import 'package:base_project_template/ui/pages/sign_in_sign_up/sign_in_sign_up_show_button_text.dart';
import 'package:base_project_template/ui/pages/sign_in_sign_up/validate/validate_confirm_password.dart';
import 'package:base_project_template/ui/pages/sign_in_sign_up/validate/validate_email.dart';
import 'package:base_project_template/ui/pages/sign_in_sign_up/validate/validate_password.dart';
import 'package:base_project_template/utils/clean_behavior.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class SignInSignUpPage extends StatefulWidget {
  @override
  _SignInSignUpPageState createState() => _SignInSignUpPageState();
}

class _SignInSignUpPageState extends State<SignInSignUpPage> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController = TextEditingController();
  SingInSingUpDictionary? languageTextSingInSingUp = FlutterDictionary.instance.language?.singInSingUpDictionary ?? en.singInSingUpDictionary;
  late final FirebaseMessaging _messaging;
  NotificationModel? _notificationInfo;
  bool obscureText = true;

  String? validatorEmail;
  String? validatorPassword;
  String? validatorConfirmPassword;

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    registerNotification();
    super.initState();
  }


  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      navigationButtonBar: false,
      needAppBar: false,
      popAppBar: '_',
      titleAppBar: '_',
      body: ScrollConfiguration(
        behavior: CleanBehavior(),
        child: SingleChildScrollView(
          child: StoreConnector<AppState, SignInSignUpViewModel>(
            converter: SignInSignUpViewModel.fromStore,
            builder: (context, vm) => Container(
              alignment: Alignment.center,
              color: AppColors.white,
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    SignInSignUpButton(
                      callBack: () {
                        validatorEmail = '';
                        validatorPassword = '';
                        validatorConfirmPassword = '';
                      },
                    ),
                    Text(
                      vm.needValue ? languageTextSingInSingUp!.signIn : languageTextSingInSingUp!.signUp,
                      style: FontStyles.textBold.copyWith(fontSize: 24.0, color: AppColors.blackTwo),
                    ),
                    const SizedBox(height: 10.0),
                    CustomTextField(
                      obscureText: false,
                      color: AppColors.pinkishGrey,
                      textFieldLoaderAndShow: false,
                      controller: _emailController,
                      hintTextField: languageTextSingInSingUp!.email,
                      iconSearch: false,
                      textFieldInContainer: true,
                      validator: (String? emailText) {
                        setState(
                          () {
                            validatorEmail = validateLongEmail(emailText!);
                          },
                        );
                      },
                    ),
                    const SizedBox(height: 10.0),
                    validatorEmail != null && validatorEmail != ''
                        ? Text(
                            validatorEmail!,
                            style: FontStyles.textBold.copyWith(fontSize: 14.0, color: AppColors.pastelRed),
                          )
                        : SizedBox(),
                    CustomTextField(
                      obscureText: vm.needOneShowValue,
                      color: AppColors.pinkishGrey,
                      textFieldLoaderAndShow: true,
                      controller: _passwordController,
                      hintTextField: languageTextSingInSingUp!.password,
                      iconSearch: false,
                      textFieldInContainer: true,
                      endTextField: SignInSingUpShowButtonText(
                        numberShow: 1,
                      ),
                      validator: (String? passwordText) {
                        setState(() {
                          validatorPassword = validatePassword(passwordText!);
                        });
                      },
                    ),
                    const SizedBox(height: 10.0),
                    validatorPassword != null && validatorPassword != ''
                        ? Text(
                            validatorPassword!,
                            style: FontStyles.textBold.copyWith(fontSize: 14.0, color: AppColors.pastelRed),
                          )
                        : SizedBox(),
                    !vm.needValue
                        ? CustomTextField(
                            obscureText: vm.needTwoShowValue,
                            color: AppColors.pinkishGrey,
                            textFieldLoaderAndShow: true,
                            controller: _confirmPasswordController,
                            hintTextField: languageTextSingInSingUp!.confirmPassword,
                            iconSearch: false,
                            textFieldInContainer: true,
                            endTextField: SignInSingUpShowButtonText(
                              numberShow: 2,
                            ),
                            validator: (String? confirmPasswordText) {
                              setState(() {
                                validatorConfirmPassword = validateConfirmPassword(confirmPasswordText!, _passwordController.text);
                              });
                            },
                          )
                        : SizedBox(),
                    validatorConfirmPassword != null && validatorConfirmPassword != '' ? const SizedBox(height: 10.0) : SizedBox(),
                    !vm.needValue && validatorConfirmPassword != null
                        ? Text(
                            validatorConfirmPassword!,
                            style: FontStyles.textBold.copyWith(fontSize: 14.0, color: AppColors.pastelRed),
                          )
                        : SizedBox(),
                    const SizedBox(height: 12.0),
                    SignInRegisterGoogleAndAppleField(
                      text: vm.needValue ? languageTextSingInSingUp!.signInWithGoogle : languageTextSingInSingUp!.registerWithGoogle,
                      icon: ImageAssetsPng.googleLogo,
                      onTap: () {},
                    ),
                    const SizedBox(height: 10.0),
                    Platform.isIOS
                        ? SignInRegisterGoogleAndAppleField(
                            text: vm.needValue ? languageTextSingInSingUp!.signInWithApple : languageTextSingInSingUp!.registerWithApple,
                            icon: ImageAssetsPng.appleLogo,
                            onTap: () {},
                          )
                        : SizedBox(),
                    const SizedBox(height: 30.0),
                    BottomButton(
                      title: vm.needValue ? languageTextSingInSingUp!.logIn : languageTextSingInSingUp!.signUp,
                      onTap: () {
                        _formKey.currentState!.validate();

                        if (vm.needValue && _emailController.text.isNotEmpty && _emailController.text.length >= 12) {
                          vm.login(_emailController.text, _passwordController.text);
                        } else if (!vm.needValue && _emailController.text.isNotEmpty && _emailController.text.length >= 12) {
                          vm.register(_emailController.text, _passwordController.text, _confirmPasswordController.text);
                        }
                      },
                    ),
                    const SizedBox(
                      height: 8.0,
                    ),
                    vm.needValue
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(12.0),
                            child: Material(
                              borderRadius: BorderRadius.circular(12.0),
                              color: Colors.transparent,
                              child: InkWell(
                                onTap: () {
                                  DialogService.instance.show(ForgotYourPasswordDialog());
                                },
                                child: Container(
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(),
                                  width: 300.0,
                                  height: 40.0,
                                  child: Text(
                                    languageTextSingInSingUp!.forgotYourPassword,
                                    style: FontStyles.interSemiBold.copyWith(color: AppColors.marigold, fontSize: 16.0),
                                  ),
                                ),
                              ),
                            ),
                          )
                        : SizedBox(),


                    //Text('title:${_notificationInfo?.title}'),
                    //Text('body:${_notificationInfo?.body}'),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget getTextField({
    required TextEditingController controller,
    required String hint,
    required FormFieldValidator<String>? validator,
    bool obscureText = false,
    bool textFieldLoaderAndShow = true,
  }) {
    return CustomTextField(
      obscureText: obscureText,
      color: AppColors.pinkishGrey,
      textFieldLoaderAndShow: textFieldLoaderAndShow,
      controller: controller,
      hintTextField: hint,
      iconSearch: false,
      textFieldInContainer: true,
      validator: validator,
    );
  }

  void registerNotification() async {
    try{
      // FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      //   final NotificationModel notification = NotificationModel(
      //     title: message.notification?.title,
      //     body: message.notification?.body,
      //   );
      //   setState(() {
      //     _notificationInfo = notification;
      //   });
      // });
      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        final NotificationModel notification = NotificationModel(
          title: message.notification?.title,
          body: message.notification?.body,
        );
        setState(() {
          _notificationInfo = notification;
          print(_notificationInfo?.title);
        });
      });
    }catch(e){
      print(e);
    }
    _messaging = FirebaseMessaging.instance;
  }
}

class NotificationModel {
  final String? title;
  final String? body;

  NotificationModel({
    required this.title,
    required this.body,
  });
}

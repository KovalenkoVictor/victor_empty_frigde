import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';

import 'package:base_project_template/ui/layouts/custom_text_field/custom_text_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SignInRegisterGoogleAndAppleField extends StatelessWidget {
  String text;
  Function onTap;
  String icon;

  SignInRegisterGoogleAndAppleField({
    required this.text,
    required this.icon,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return CustomTextField(
      obscureText: true,
      color: AppColors.pinkishGrey,
      textFieldLoaderAndShow: false,
      iconSearch: false,
      textFieldInContainer: false,
      widget: Material(
        color: AppColors.transparent,
        child: InkWell(
          borderRadius: BorderRadius.circular(12.0),
          onTap: onTap as void Function()?,
          child: Container(
            child: Row(
              children: [
                SizedBox(width: mediaQuery.width * 0.15),
                Image.asset(
                  icon,
                  width: 22.0,
                ),
                SizedBox(width: mediaQuery.width * 0.15),
                Text(
                  text,
                  style: FontStyles.textRegular.copyWith(color: AppColors.black6, fontSize: 16.0),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

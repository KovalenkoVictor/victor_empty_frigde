import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/dictionary_classes/sign_in_sign_up_dictionary.dart';
import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/sign_in_sign_up_state/sign_in_sign_up_vm.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class SignInSingUpShowButtonText extends StatelessWidget {
  int numberShow;

  // ignore: use_key_in_widget_constructors
  SignInSingUpShowButtonText({required this.numberShow});

  SingInSingUpDictionary? languageTextSingInSingUp = FlutterDictionary.instance.language?.singInSingUpDictionary ?? en.singInSingUpDictionary;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, SignInSignUpViewModel>(
      converter: SignInSignUpViewModel.fromStore,
      builder: (context, vm) => Padding(
        padding: const EdgeInsets.only(right:8.0),
        child: Material(
          color: AppColors.transparent,
          child: InkWell(
            borderRadius: BorderRadius.circular(10.0),
            onTap: () {
              if (numberShow == 1) {
                vm.changeOneShowValue();
              } else if (numberShow == 2) {
                vm.changeTwoShowValue();
              }
              else if (numberShow == 3) {
                vm.changeThreeShowValue();
              }
              else if (numberShow == 4) {
                vm.changeFourShowValue();
              }
            },
            child: Container(
              alignment: Alignment.center,

              height: 30.0,
              child: numberShow == 1
                  ? Text(
                      vm.needOneShowValue ? languageTextSingInSingUp!.show : languageTextSingInSingUp!.hide,
                      style: FontStyles.textRegular.copyWith(color: AppColors.marigold, fontSize: 16.0),
                maxLines: 1,
                    ):numberShow == 2
                  ? Text(
                      vm.needTwoShowValue ? languageTextSingInSingUp!.show : languageTextSingInSingUp!.hide,
                      style: FontStyles.textRegular.copyWith(color: AppColors.marigold, fontSize: 16.0),
                      overflow: TextOverflow.fade,
                    ):numberShow == 3
                  ?Text(
                vm.needThreeShowValue ? languageTextSingInSingUp!.show : languageTextSingInSingUp!.hide,
                style: FontStyles.textRegular.copyWith(color: AppColors.marigold, fontSize: 16.0),
              ):Text(
                vm.needFourShowValue ? languageTextSingInSingUp!.show : languageTextSingInSingUp!.hide,
                style: FontStyles.textRegular.copyWith(color: AppColors.marigold, fontSize: 16.0),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

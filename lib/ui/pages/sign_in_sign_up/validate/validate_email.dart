import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/dictionary_classes/sign_in_sign_up_dictionary.dart';
import 'package:base_project_template/dictionary/flutter_dictionary.dart';

bool validateEmail(String value) {
  final RegExp regexp = RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
  // ignore: avoid_bool_literals_in_conditional_expressions
  return (!regexp.hasMatch(value)) ? false : true;
}


  String validateLongEmail (String text){
  final SingInSingUpDictionary? languageTextSingInSingUp = FlutterDictionary.instance.language?.singInSingUpDictionary ?? en.singInSingUpDictionary;
  if (text.isEmpty) {
   return languageTextSingInSingUp!.pleaseEnterYourEmail;
  } else if (text.length < 12) {
  return languageTextSingInSingUp!.enterMoreThanCharacters;
  } else if (validateEmail(text) == false) {
  return languageTextSingInSingUp!.emailIsNotEnteredCorrectly;
  }
  return '';
}
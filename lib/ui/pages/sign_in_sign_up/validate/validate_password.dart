import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/dictionary_classes/sign_in_sign_up_dictionary.dart';
import 'package:base_project_template/dictionary/flutter_dictionary.dart';

String validatePassword(String text){
  final SingInSingUpDictionary? languageTextSingInSingUp = FlutterDictionary.instance.language?.singInSingUpDictionary ?? en.singInSingUpDictionary;
  if (text.isEmpty || text.isEmpty) {
   return languageTextSingInSingUp!.pleaseEnterPassword;
  } else if (text.length < 6) {
   return languageTextSingInSingUp!.enterMoreThanPasswordCharacters;
  }
  return '';
}
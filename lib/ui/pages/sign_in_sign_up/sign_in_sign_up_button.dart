import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/dictionary_classes/sign_in_sign_up_dictionary.dart';
import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/sign_in_sign_up_state/sign_in_sign_up_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class SignInSignUpButton extends StatefulWidget {

  void Function() callBack;
  SignInSignUpButton({
    required this.callBack,
});

  @override
  _SignInSignUpButtonState createState() => _SignInSignUpButtonState();
}

class _SignInSignUpButtonState extends State<SignInSignUpButton> {
  bool singInSignUpTextColor = true;
  SingInSingUpDictionary? languageTextSingInSingUp = FlutterDictionary.instance.language?.singInSingUpDictionary ?? en.singInSingUpDictionary;
  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return StoreConnector<AppState, SignInSignUpViewModel>(
      converter: SignInSignUpViewModel.fromStore,
      builder: (context, vm) => Container(
        margin: EdgeInsets.only(top: mediaQuery.height * 0.11, bottom: 26.0, left: 14.0, right: 14.0),
        height: 50.0,
        decoration: BoxDecoration(
          color: AppColors.whiteThree,
          border: Border.all(color: AppColors.whiteTwo, width: 1.2),
          borderRadius: BorderRadius.circular(24.0),
        ),
        child: Material(
          color: AppColors.transparent,
          child: InkWell(
            borderRadius: BorderRadius.circular(360.0),
            onTap: () {
              vm.changeValue();
                widget.callBack();
              if(!vm.needOneShowValue){
                vm.changeOneShowValue();
              }
              if(!vm.needTwoShowValue){
                vm.changeTwoShowValue();
              }

            },
            child: Row(
              children: [
                Stack(
                  children: [
                    AnimatedPositioned(
                      curve: Curves.fastOutSlowIn,
                      duration: Duration(seconds: 1),
                      width: mediaQuery.width * 0.5 - 14.0,
                      height: 46.0,
                      left: !vm.needValue ? mediaQuery.width * 0.5 - 18.0 : 0,
                      child: Material(
                        color: AppColors.white,
                        borderRadius: BorderRadius.circular(360.0),
                        child: InkWell(
                          onTap: () {
                            vm.changeValue();
                          },
                          borderRadius: BorderRadius.circular(360.0),
                          child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(24.0),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: mediaQuery.width * 1 - 31.0,
                      height: 46,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          IgnorePointer(
                            child: Text(
                              languageTextSingInSingUp!.signIn,
                              style:
                                  FontStyles.interSemiBold.copyWith(color: vm.needValue ? AppColors.marigold : AppColors.pinkishGrey, fontSize: 16.0),
                            ),
                          ),
                          SizedBox(width: mediaQuery.width*0.3),

                          IgnorePointer(
                            child: ConstrainedBox(
                              constraints: BoxConstraints(
                                maxWidth: 120.0
                              ),
                              child: Text(
                                languageTextSingInSingUp!.signUp,
                                style: FontStyles.interSemiBold
                                    .copyWith(color: !vm.needValue ? AppColors.marigold : AppColors.pinkishGrey, fontSize: 16.0),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

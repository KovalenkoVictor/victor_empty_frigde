import 'dart:async';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/const.dart';
import 'package:base_project_template/res/image_assets.dart';
import 'package:base_project_template/ui/pages/sign_in_sign_up/sign_in_sign_up_page.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';

// ignore: use_key_in_widget_constructors
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool loader = false;

  final timeout = Duration(seconds: 5);

  @override
  void initState() {
    super.initState();
    Future.delayed(
      SECONDS_5,
      () {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => SignInSignUpPage(),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        color: AppColors.wheat.withOpacity(0.4),
        child: Center(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(top: 93.0),
                width: mediaQuery.width * 0.72,
                height: mediaQuery.height * 0.72,
                child: Image.asset(ImageAssetsPng.splashScreen),
              ),
              const SizedBox(height: 40.0),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 48.0),
                child: LinearPercentIndicator(
                  animationDuration: 3500,
                  lineHeight: 8.0,
                  percent: 1.0,
                  animation: true,
                  backgroundColor: AppColors.white,
                  linearStrokeCap: LinearStrokeCap.roundAll,
                  progressColor: AppColors.marigold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

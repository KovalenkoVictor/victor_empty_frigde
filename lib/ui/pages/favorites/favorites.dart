import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/dictionary_classes/app_bar_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/favorites_dictionary.dart';
import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/res/image_assets.dart';
import 'package:base_project_template/services/network_service/dto/recipe_dto/recipe_dto.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/user_state/user_vm.dart';
import 'package:base_project_template/ui/layouts/card_recipes/card_recipes.dart';
import 'package:base_project_template/ui/layouts/main_layout/main_layout.dart';
import 'package:base_project_template/ui/pages/detail_cards_page/detail_cards_vm.dart';
import 'package:base_project_template/utils/clean_behavior.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

// ignore: must_be_immutable, use_key_in_widget_constructors
class FavoritesPage extends StatelessWidget {
  AppBarDictionary? languageTextAppBar = FlutterDictionary.instance.language?.appBarDictionary ?? en.appBarDictionary;
  FavoritesDictionary? languageTexFavorites = FlutterDictionary.instance.language?.favoritesDictionary ?? en.favoritesDictionary;

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return MainLayout(
      titleAppBar: '${languageTextAppBar!.favorites}',
      popAppBar: '${languageTextAppBar!.back}',
      needAppBar: true,
      navigationButtonBar: true,
      body: StoreConnector<AppState, UserViewModel>(
        converter: UserViewModel.fromStore,
        builder: (context, vm) => StoreConnector<AppState, DetailCardsViewModel>(
          converter: DetailCardsViewModel.fromStore,
          builder: (context, vmDetailCards) => Container(
            color: AppColors.white,
            child: Center(
              child: vm.listFavorites.isNotEmpty
                  ? ScrollConfiguration(
                behavior: CleanBehavior(),
                    child: ListView.builder(
                        itemCount: vm.listFavorites.length,
                        itemBuilder: (context,index) {
                          final json = vm.listFavorites[index].toJson();
                          json['ingredients']= vm.listFavorites[index].ingredients.map((e) => e.toJson()).toList();

                          return CardRecipes(
                            buttonFavorites: false,
                            detailCardCarouselSlider: false,
                            recipe:RecipeDto.fromJson(json),
                            favoritePageOrRecipe:false,
                            index:index,
                          );
                        }),
                  )
                  : Column(
                      children: [
                        Spacer(),
                        Text(
                          languageTexFavorites!.yourListIsEmpty,
                          style: FontStyles.textBold.copyWith(fontSize: 24.0, color: AppColors.black6),
                        ),
                        Spacer(),
                        Container(
                          margin: EdgeInsets.only(left: 30.0),
                          height: mediaQuery.height * 0.5,
                          child: Image.asset(
                            ImageAssetsPng.favoriteChefTwo,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ],
                    ),
            ),
          ),
        ),
      ),
    );
  }
}

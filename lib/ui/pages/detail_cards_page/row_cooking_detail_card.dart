import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class RowCookingDetailCard extends StatelessWidget{
  String text;
  // ignore: use_key_in_widget_constructors
  RowCookingDetailCard({
    required this.text,
});
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ShaderMask(
          shaderCallback: (Rect bounds) {
            return RadialGradient(
              center: Alignment.topCenter,
              radius: 1,
              colors: <Color>[
                AppColors.wheat,
                AppColors.marigold,
              ],
            ).createShader(bounds);
          },
          child: Icon(
            Icons.delete,
            color: AppColors.white,
            size: 30.0,
          ),
        ),
        Text(
         text ,
          style: FontStyles.textRegular.copyWith(color: AppColors.black6, fontSize: 20.0),
        ),
      ],
    );
  }
}
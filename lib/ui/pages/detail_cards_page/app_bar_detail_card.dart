import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/dictionary_classes/app_bar_dictionary.dart';
import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/app_styles/app_gradient.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/res/image_assets.dart';
import 'package:base_project_template/services/network_service/dto/favorites_dto/favorites_dto.dart';
import 'package:base_project_template/services/network_service/dto/recipe_dto/recipe_dto.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/route_service/route_vm.dart';
import 'package:base_project_template/store/shared/user_state/user_vm.dart';
import 'package:base_project_template/ui/pages/detail_cards_page/detail_cards_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

// ignore: must_be_immutable
class AppBarDetailCard extends StatefulWidget {
  bool appBarOpenAndClose;
 final RecipeDto recipe;
 final List<FavoritesDto> listFavorites;
 final int index;
  bool recipeOrFavorites;

  // ignore: use_key_in_widget_constructors
  AppBarDetailCard({
    required this.appBarOpenAndClose,
    required this.recipe,
    required this.listFavorites,
    required this.index,
    required this.recipeOrFavorites,

  });

  @override
  _AppBarDetailCardState createState() => _AppBarDetailCardState();
}

class _AppBarDetailCardState extends State<AppBarDetailCard> {
  bool addFavoriteRecipe = false;
  bool recipeButtonFavorite = false;

  AppBarDictionary? languageTextAppBar = FlutterDictionary.instance.language?.appBarDictionary ?? en.appBarDictionary;

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    recipeButtonFavorite = widget.listFavorites.any((id) => widget.recipe.i == id.i);

    return StoreConnector<AppState, UserViewModel>(
      converter: UserViewModel.fromStore,
      builder: (context, vmUser) => StoreConnector<AppState, DetailCardsViewModel>(
        converter: DetailCardsViewModel.fromStore,
        builder: (context, vmDetailCards) => SliverAppBar(
          pinned: true,
          backgroundColor: AppColors.transparent,
          expandedHeight: mediaQuery.height * 0.35,
          leadingWidth: mediaQuery.width * 0.3,
          flexibleSpace: SizedBox(
            child: Stack(
              children: [
                AnimatedContainer(
                  duration: Duration(seconds: 1),
                  color: Colors.white,
                  width: double.infinity,
                  height: mediaQuery.height,
                  child: widget.appBarOpenAndClose
                      ? Image.network(
                          widget.recipe.image,
                          fit: BoxFit.cover,
                          errorBuilder: (BuildContext? context, Object? exception, StackTrace? stackTrace) {
                            return Container(
                              decoration: BoxDecoration(
                                gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [
                                  AppColors.blackTwo.withOpacity(0.5),
                                  AppColors.blackTwo.withOpacity(0.3),
                                ]),
                              ),
                              child: Image.asset(
                                ImageAssetsPng.manYellow,
                              ),
                            );
                          },
                        )
                      : Container(
                          decoration: BoxDecoration(
                            gradient: AppGradient.wheatMarigold,
                          ),
                        ),
                ),
                Positioned(
                  bottom: -4,
                  child: Container(
                    padding: EdgeInsets.only(left: 16.0),
                    width: mediaQuery.width,
                    height: mediaQuery.height * 0.098,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      border: Border.all(color: AppColors.transparent),
                      color: widget.appBarOpenAndClose ? AppColors.white : AppColors.transparent,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10.0),
                        topRight: Radius.circular(10.0),
                      ),
                    ),
                  ),
                ),
                AnimatedPositioned(
                  bottom: widget.appBarOpenAndClose ? 10.0 : 16.0,
                  left: widget.appBarOpenAndClose
                      ? 16.0
                      : FlutterDictionary.instance.isRTL
                          ? mediaQuery.width * -0.2
                          : mediaQuery.width * 0.22,
                  duration: Duration(milliseconds: 500),
                  child: AnimatedContainer(
                    duration: Duration(milliseconds: 500),
                    width: mediaQuery.width,
                    child: Row(
                      children: [
                        Container(
                          padding: EdgeInsets.only(right: FlutterDictionary.instance.isRTL ? 34.0 : 0),
                          alignment: widget.appBarOpenAndClose
                              ? FlutterDictionary.instance.isRTL
                                  ? Alignment.bottomRight
                                  : Alignment.bottomLeft
                              : Alignment.center,
                          width: widget.appBarOpenAndClose ? mediaQuery.width * 0.75 : mediaQuery.width * 0.54,
                          child: Text(
                            widget.recipe.name,
                            textAlign: TextAlign.center,
                            style: FontStyles.textBold.copyWith(
                                color: widget.appBarOpenAndClose ? AppColors.black6 : AppColors.white,
                                fontSize: 24.0,
                                shadows: [
                                  BoxShadow(color: AppColors.blackTwo.withOpacity(0.25), blurRadius: 8, offset: Offset(0, 4), spreadRadius: 10)
                                ]),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                ),
                AnimatedPositioned(
                  duration: Duration(milliseconds: 500),
                  bottom: widget.appBarOpenAndClose ? 40.0 : 10.0,
                  right: FlutterDictionary.instance.isRTL ? mediaQuery.width * 0.8 : 14.0,
                  child: StoreConnector<AppState, RouteViewModel>(
                    converter: RouteViewModel.fromStore,
                    builder: (context, route) => InkWell(
                      onTap: () {
                        setState(() {
                          if (!recipeButtonFavorite){
                              vmUser.addFavorite(vmUser.token,widget.recipe.i.toString());
                          }else{
                              vmUser.removeFavorite(vmUser.token,widget.recipe.i.toString());
                          }
                          recipeButtonFavorite = !recipeButtonFavorite;
                        });
                      },
                      child: AnimatedContainer(
                        duration: Duration(milliseconds: 500),
                        alignment: Alignment.center,
                        width: widget.appBarOpenAndClose ? 70.0 : 44.0,
                        height: widget.appBarOpenAndClose ? 70.0 : 44.0,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          gradient: LinearGradient(
                              stops: [0.2, 1],
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                              colors: [
                                widget.appBarOpenAndClose ? AppColors.marigold : Color(0x00ffffff),
                                widget.appBarOpenAndClose ? AppColors.wheat : Color(0x00ffffff),
                              ]),
                        ),
                        child: Icon(
                          Icons.favorite,
                          size: 44.0,
                          color: addFavoriteRecipe || recipeButtonFavorite ? AppColors.red.withOpacity(0.5) : AppColors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          leading: Stack(
            children: [
              AnimatedPositioned(
                duration: Duration(milliseconds: 500),
                bottom: widget.appBarOpenAndClose ? 8.0 : 16.0,
                child: Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: FlutterDictionary.instance.isRTL ? 0 : 16.0, right: FlutterDictionary.instance.isRTL ? 16.0 : 0),
                      child: Material(
                        color: AppColors.transparent,
                        child: StoreConnector<AppState, RouteViewModel>(
                          converter: RouteViewModel.fromStore,
                          builder: (context, route) => InkWell(
                            borderRadius: BorderRadius.circular(4.0),
                            onTap: () {
                              route.pop();
                            },
                            child: Row(
                              children: [
                                Icon(
                                  Icons.arrow_back_ios,
                                  size: 28.0,
                                ),
                                Transform.translate(
                                  offset: FlutterDictionary.instance.isRTL ? Offset(8, 0) : Offset(-8, 0),
                                  child: Text(
                                    languageTextAppBar!.back,
                                    style: FontStyles.textRegular.copyWith(
                                      fontSize: 16.0,
                                      color: AppColors.white,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/dictionary_classes/detail_cards_dictionary.dart';
import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/services/network_service/dto/recipe_dto/recipe_dto.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/user_state/user_vm.dart';
import 'package:base_project_template/ui/layouts/main_layout/main_layout.dart';
import 'package:base_project_template/ui/pages/detail_cards_page/app_bar_detail_card.dart';
import 'package:base_project_template/ui/pages/detail_cards_page/bon_appetit_image_and_text_detail_card.dart';
import 'package:base_project_template/ui/pages/detail_cards_page/detail_cards_vm.dart';
import 'package:base_project_template/ui/pages/detail_cards_page/have_ingredients_detail_card.dart';
import 'package:base_project_template/ui/pages/detail_cards_page/miss_ingredients_detail_card.dart';
import 'package:base_project_template/ui/pages/detail_cards_page/row_cooking_detail_card.dart';
import 'package:base_project_template/ui/pages/detail_cards_page/similar_recipes_detail_cards.dart';
import 'package:base_project_template/ui/pages/detail_cards_page/steps_detail_card.dart';
import 'package:base_project_template/utils/clean_behavior.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

// ignore: use_key_in_widget_constructors
class DetailCardsPage extends StatefulWidget {
  final RecipeDto recipe;

  // ignore: use_key_in_widget_constructors
  DetailCardsPage({
    required this.recipe,
  });

  @override
  _DetailCardsPageState createState() => _DetailCardsPageState();
}



class _DetailCardsPageState extends State<DetailCardsPage> {

  bool addFavoriteRecipe = false;
  bool appBarOpenAndClose = true;
  final DetailCardsDictionary languageTextDetailCard = FlutterDictionary.instance.language?.detailCardsDictionary ?? en.detailCardsDictionary!;
  late final ScrollController _controller;

  void _scrollListener() {
    if (_controller.offset >= 220) {
      setState(() {
        appBarOpenAndClose = false;
      });
    } else {
      setState(() {
        appBarOpenAndClose = true;
      });
    }
  }

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return MainLayout(
      titleAppBar: '_',
      popAppBar: '_',
      needAppBar: false,
      navigationButtonBar: true,
      body: StoreConnector<AppState, UserViewModel>(
        converter: UserViewModel.fromStore,
        builder: (context, vmUser) => SizedBox(
          child: StoreConnector<AppState, DetailCardsViewModel>(
            converter: DetailCardsViewModel.fromStore,
            builder: (context, vmDetailCards) => ScrollConfiguration(
              behavior: CleanBehavior(),
              child: CustomScrollView(
                shrinkWrap: true,
                controller: _controller,
                slivers: [
                  AppBarDetailCard(
                    recipeOrFavorites: vmUser.recipeOrFavorites,
                    appBarOpenAndClose: appBarOpenAndClose,
                    index: vmDetailCards.index,
                    listFavorites: vmUser.listFavorites,
                    recipe: widget.recipe,
                  ),
                  SliverList(
                    delegate: SliverChildListDelegate([
                      Container(
                        width: mediaQuery.width,
                        padding: EdgeInsets.only(top: 8.0),
                        color: AppColors.white,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 16.0, right: 16.0),
                              child: Text(
                                languageTextDetailCard.foodElements,
                                style: FontStyles.textRegular.copyWith(color: AppColors.black6, fontSize: 20.0),
                              ),
                            ),
                            const SizedBox(height: 16.0),
                            widget.recipe.missIngredients > 0
                                ? Padding(
                                    padding: EdgeInsets.only(left: 16.0, right: 16.0),
                                    child: Text(
                                      languageTextDetailCard.youDontHave,
                                      style: FontStyles.textRegular.copyWith(color: AppColors.pastelRed, fontSize: 16.0),
                                    ),
                                  )
                                : const SizedBox(),
                            widget.recipe.missIngredients > 0
                                ?Padding(padding: EdgeInsets.only(left: 16.0, right: 16.0), child: MissIngredientsDetailCard(recipe:widget.recipe,))
                                : SizedBox(),
                            widget.recipe.missIngredients > 0 ? const SizedBox(height: 16.0) : SizedBox(),

                            widget.recipe.hasIngredients > 0
                                ? Padding(
                                    padding: EdgeInsets.only(left: 16.0, right: 16.0),
                                    child: Text(
                                      languageTextDetailCard.youHave,
                                      style: FontStyles.textRegular.copyWith(fontSize: 16.0, color: AppColors.blackTwo),
                                    ),
                                  )
                                : SizedBox(),
                            widget.recipe.missIngredients > 0 ? const SizedBox(height: 8.0) : SizedBox(),
                            widget.recipe.hasIngredients > 0
                                ? Padding(
                                    padding: EdgeInsets.only(left: 16.0, right: 16.0),
                                    child: HaveIngredientsDetailCard(recipe: widget.recipe,),
                                  )
                                : SizedBox(),
                            const SizedBox(height: 72.0),
                            Padding(
                              padding: EdgeInsets.only(left: 16.0, right: 16.0),
                              child: RowCookingDetailCard(
                                text: languageTextDetailCard.cooking,
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(left: 16.0, right: 16.0), child: StepsDetailCard(recipe: widget.recipe,),),
                            const SizedBox(height: 44.0),
                            BonAppetitImageAndTextDetailCard(),
                            const SizedBox(height: 60.0),
                            Padding(
                              padding: EdgeInsets.only(left: 16.0, right: 16.0),
                              child: Text(
                                languageTextDetailCard.similarRecipes,
                                style: FontStyles.textBold.copyWith(fontSize: 24.0, color: AppColors.blackTwo),
                              ),
                            ),
                            SimilarRecipesDetailCards(),
                            const SizedBox(height: 70.0),
                          ],
                        ),
                      ),
                    ]),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

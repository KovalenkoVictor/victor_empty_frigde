import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/res/image_assets.dart';
import 'package:base_project_template/services/network_service/dto/ingredient_dto/ingredient_dto.dart';
import 'package:base_project_template/services/network_service/dto/recipe_dto/recipe_dto.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/user_state/user_vm.dart';
import 'package:base_project_template/ui/pages/detail_cards_page/detail_cards_vm.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

// ignore: use_key_in_widget_constructors
class HaveIngredientsDetailCard extends StatelessWidget {
  final RecipeDto recipe;

  // ignore: use_key_in_widget_constructors
  HaveIngredientsDetailCard({
    required this.recipe,
});

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, UserViewModel>(
      converter: UserViewModel.fromStore,
      builder: (context, vmUser){
        return StoreConnector<AppState, DetailCardsViewModel>(
          converter: DetailCardsViewModel.fromStore,
          builder: (context, vmDetailCards){
            final List<IngredientDto> _listHaveIngredient=vmUser.allIngredients
                .where((ingredient) => recipe.ingredients.any((ingredientInRecipe) => int.parse(ingredientInRecipe.i) == ingredient.i))
                .where((ingredient) => vmUser.ids.any((id) => int.parse(id) == ingredient.i)).toList();

            return  Column(
              children:_listHaveIngredient
                  .map(
                    (e) => Padding(
                  padding: EdgeInsets.only(top: 10.0),
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 10.0),
                        width: 32.0,
                        height: 32.0,
                        child: Image.network(
                          e.image,
                          errorBuilder: (BuildContext? context, Object? exception, StackTrace? stackTrace) {
                            return Image.asset(
                              ImageAssetsPng.manYellowTwo,
                              fit: BoxFit.cover,
                            );
                          },
                        ),
                      ),
                      Text(
                        '${e.name}',
                        style: FontStyles.textRegular.copyWith(color: AppColors.blackTwo, fontSize: 16.0),
                      ),
                      Spacer(),
                      Text(
                        '${e.count} ',
                        style: FontStyles.textRegular.copyWith(color: AppColors.blackTwo, fontSize: 12.0),
                      ),
                      Text(
                        '${e.description}',
                        style: FontStyles.textRegular.copyWith(color: AppColors.blackTwo, fontSize: 12.0),
                      ),
                    ],
                  ),
                ),
              ).toList(),
            );
          }
        );
      }
    );
  }
}

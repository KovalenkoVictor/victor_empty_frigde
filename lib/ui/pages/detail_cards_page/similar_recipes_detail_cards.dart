import 'package:base_project_template/services/network_service/dto/recipe_dto/recipe_dto.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/user_state/user_vm.dart';
import 'package:base_project_template/ui/layouts/card_recipes/card_recipes.dart';
import 'package:base_project_template/utils/clean_behavior.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_redux/flutter_redux.dart';

// ignore: use_key_in_widget_constructors
class SimilarRecipesDetailCards extends StatelessWidget {
  late dynamic listFavorites;

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return StoreConnector<AppState, UserViewModel>(
      converter: UserViewModel.fromStore,
      builder: (context, vmUser) => SizedBox(
        width: mediaQuery.width,
        height: mediaQuery.height * 0.19,
        child: ScrollConfiguration(
          behavior: CleanBehavior(),
          child: ListView.builder(
              padding: EdgeInsets.zero,
              scrollDirection: Axis.horizontal,
              itemCount: vmUser.recipeOrFavorites ? vmUser.recipe.length : vmUser.listFavorites.length,
              itemBuilder: (context, index) {
                if (!vmUser.recipeOrFavorites) {
                  final json = vmUser.listFavorites[index].toJson();
                  json['ingredients'] = vmUser.listFavorites[index].ingredients.map((e) => e.toJson()).toList();
                  listFavorites = json;
                }
                return vmUser.recipeOrFavorites
                    ? CardRecipes(
                        buttonFavorites: true,
                        detailCardCarouselSlider: true,
                        recipe: vmUser.recipe[index],
                        favoritePageOrRecipe: true,
                        index: index,
                      )
                    : CardRecipes(
                        buttonFavorites: true,
                        detailCardCarouselSlider: true,
                        favoritePageOrRecipe: false,
                        recipe: RecipeDto.fromJson(listFavorites),
                        index: index,
                      );
              }),
        ),
      ),
    );
  }
}

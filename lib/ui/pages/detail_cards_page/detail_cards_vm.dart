import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/detail_cards_state/detail_cards_selector.dart';
import 'package:redux/redux.dart';

class DetailCardsViewModel {
   int index;
   Function(int index) value;

  DetailCardsViewModel({
    required this.index,
    required this.value,
  });

  static DetailCardsViewModel fromStore(Store<AppState> store) {
    return DetailCardsViewModel(
      index:DetailCardsSelector.getIndexCard(store),
      value:DetailCardsSelector.getValueNeedCard(store),
    );
  }
}
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/const.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/res/image_assets.dart';
import 'package:flutter/cupertino.dart';

// ignore: use_key_in_widget_constructors
class BonAppetitImageAndTextDetailCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return SizedBox(
      width: double.infinity,
      height: mediaQuery.height * 0.34,
      child: Stack(
        children: [
          Center(
              child: Image.asset(
            ImageAssetsPng.manYellow,
          )),
          Positioned(
            left: 0,
            child: SizedBox(
              width: mediaQuery.width * 0.52,
              height: mediaQuery.height * 0.34,
              child: Image.asset(
                ImageAssetsPng.congratLeft,
                fit: BoxFit.fill,
              ),
            ),
          ),
          Positioned(
            right: 0,
            child: SizedBox(
              width: mediaQuery.width * 0.52,
              height: mediaQuery.height * 0.34,
              child: Image.asset(
                ImageAssetsPng.congratRight,
                fit: BoxFit.fill,
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            left: -14.0,
            child: SizedBox(
              width: mediaQuery.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Spacer(),
                  SizedBox(
                    child: Text(
                      BON_APPETIT,
                      style: FontStyles.textBold.copyWith(fontSize: 30.0, color: AppColors.blackTwo),
                    ),
                  ),
                  Spacer(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/const_rout.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/res/image_assets.dart';
import 'package:base_project_template/services/network_service/dto/recipe_dto/recipe_dto.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/route_service/route_helper.dart';
import 'package:base_project_template/store/shared/user_state/user_vm.dart';
import 'package:base_project_template/ui/pages/detail_cards_page/detail_cards_vm.dart';
import 'package:base_project_template/utils/clean_behavior.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

// ignore: use_key_in_widget_constructors, must_be_immutable
class StepsDetailCard extends StatelessWidget {
  ScrollController controller = ScrollController();
  final RecipeDto recipe;

  // ignore: use_key_in_widget_constructors
  StepsDetailCard({
    required this.recipe,
});

  @override
  Widget build(BuildContext context) {

    final Size mediaQuery = MediaQuery.of(context).size;
    return StoreConnector<AppState, UserViewModel>(
      converter: UserViewModel.fromStore,
      builder: (context, vmUser) => StoreConnector<AppState, DetailCardsViewModel>(
        converter: DetailCardsViewModel.fromStore,
        builder: (context, vmDetailCards) => ListView.builder(
          padding: EdgeInsets.only(top: 15.0),
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          controller: controller,
          itemCount: recipe.steps.length,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: EdgeInsets.only(top: 10.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    alignment: Alignment.center,
                    width: 30.0,
                    height: 30.0,
                    decoration: BoxDecoration(
                      color: AppColors.transparent,
                      shape: BoxShape.circle,
                      border: Border.all(color: AppColors.black6, width: 1.5),
                    ),
                    child: Text('${index+1}',style: FontStyles.textRegular.copyWith(fontSize: 16.0, color: AppColors.black6,),),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 8.0, left: 10.0),
                    width: mediaQuery.width - 100,
                    child: Text(
                      '"${recipe.steps[index]}"',
                      style: FontStyles.textRegular.copyWith(fontSize: 16.0, color: AppColors.black6, height: 24 / 16),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}


abstract class IPageData {
  final String id;

  IPageData({
    required this.id
  });
}
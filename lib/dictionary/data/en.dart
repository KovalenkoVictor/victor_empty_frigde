import 'package:base_project_template/dictionary/dictionary_classes/app_bar_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/bottom_navigation_bar_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/detail_cards_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/favorites_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/home_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/on_boarding_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/pop_up_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/recipes_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/settings_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/sign_in_sign_up_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/text_field_dictionary.dart';
import 'package:base_project_template/dictionary/models/language.dart';

Language en = Language(
  onBoardingPage: OnBoardingDictionary(
      titleTextOneOnBoarding: 'Choose the products you have',
      titleTextTwoOnBoarding: 'Pick out all the groceries and click "watch recipes."',
      titleTextThreeOnBoarding: 'Select a recipe from the list provided.',
      titleTextFourOnBoarding: 'Follow the recipe and bon appetit',
      buttonTextNext: 'Next',
      buttonTextStart: 'Start!'),
  appBarDictionary: AppBarDictionary(back: 'Back', favorites: 'Favorites', settings: 'Settings', notification: 'Notification', recipes: 'Recipes'),
  bottomNavigationBarDictionary: BottomNavigationBarDictionary(home: 'Home', favorites: 'Favorites', settings: 'Settings'),
  settingsDictionary: SettingsDictionary(
    logOut: 'Log out',
    language: 'Language',
    notification: 'Notification',
    aboutCompany: 'About company',
  ),
  textFieldDictionary: TextFieldDictionary(chooseTheFood: 'Choose the food', notFound: 'Not Found'),
  favoritesDictionary: FavoritesDictionary(yourListIsEmpty: 'Your list is empty'),
  singInSingUpDictionary: SingInSingUpDictionary(
    signIn: 'Sign In',
    signUp: 'Sign Up',
    logIn: 'Log In',
    email: 'Email',
    password: 'Password',
    confirmPassword: 'Confirm Password',
    show: 'Show',
    signInWithApple: 'Sign In with Apple',
    registerWithApple: 'Register with Apple',
    signInWithGoogle: 'Sign In with Google',
    registerWithGoogle: 'Register with Google',
    forgotYourPassword: 'Forgot your password?',
    emailIsNotEnteredCorrectly: 'E-mail is not entered correctly',
    enterMoreThanCharacters: 'Enter more than 12 characters',
    enterMoreThanPasswordCharacters: 'Enter more than 6 characters',
    passwordDoesNotMatch: 'Password does not match',
    pleaseEnterPassword: 'Please enter password',
    pleaseEnterYourEmail: 'Please enter your email',
    hide: 'Hide',
  ),
  popUpDictionary: PopUpDictionary(
    logOutTitle: 'Are you sure you want to quit the application ',
    logOutNo: 'No',
    logOutYes: 'Yes',
    tutorialTitle: 'You can delete food by swipe to the right or left',
    ok: 'Ok!',
    notificationTitle: '-',
    notificationBody: '-',
    favoritesDelete: 'This recipe was removed from the favorites',
    favoritesAdd: 'This recipe add to favorites',
    errorInternetConnection: 'There\'s been a bug on the server, please wait..',
    alreadyRegistered: 'Such a user is already registered',
  ),
  homeDictionary: HomeDictionary(clearAll: 'Clear all', watchRecipe: 'Watch recipe'),
  recipesDictionary: RecipesDictionary(
    yourListIsEmpty: 'Your list is empty',
    youDontHave: 'You don\'t have:',
  ),
  detailCardsDictionary: DetailCardsDictionary(foodElements: 'Food elements:', youDontHave: 'You dont have:',
      youHave: 'You have:', cooking: 'Cooking:', similarRecipes: 'Similar recipes'),
);

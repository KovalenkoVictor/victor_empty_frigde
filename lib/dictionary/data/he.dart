import 'package:base_project_template/dictionary/dictionary_classes/app_bar_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/bottom_navigation_bar_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/detail_cards_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/favorites_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/home_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/on_boarding_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/pop_up_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/recipes_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/settings_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/sign_in_sign_up_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/text_field_dictionary.dart';
import 'package:base_project_template/dictionary/models/language.dart';

Language he = Language(
  onBoardingPage: OnBoardingDictionary(
      titleTextOneOnBoarding: 'בחר את המוצרים שיש לך',
      titleTextTwoOnBoarding: 'בחר את כל המצרכים ולחץ על "צפה במתכונים".',
      titleTextThreeOnBoarding: 'בחר מתכון מהרשימה המופיעה.',
      titleTextFourOnBoarding: 'עקוב אחר המתכון ובתאבון',
      buttonTextNext: 'הַבָּא',
      buttonTextStart: 'הַתחָלָה'),
  appBarDictionary: AppBarDictionary(back: 'חזור', favorites: 'מועדפים', settings: 'הגדרות', notification: 'הוֹדָעָה', recipes: 'מתכונים'),
  bottomNavigationBarDictionary: BottomNavigationBarDictionary(home: 'בית', favorites: 'מועדפים', settings: 'הגדרות'),
  settingsDictionary: SettingsDictionary(
    logOut: 'להתנתק',
    language: 'שפה',
    notification: 'הוֹדָעָה',
    aboutCompany: 'אודות חברה',
  ),
  textFieldDictionary: TextFieldDictionary(chooseTheFood: 'בחר את האוכל', notFound: 'לא נמצא'),
  favoritesDictionary: FavoritesDictionary(yourListIsEmpty: 'הרשימה שלך ריקה'),
  singInSingUpDictionary: SingInSingUpDictionary(
    signIn: 'להתחבר',
    signUp: 'הירשם',
    logIn: 'התחברות',
    email: 'אימייל',
    password: 'סיסמה',
    confirmPassword: 'אשר סיסמה',
    show: 'הופעה',
    signInWithApple: 'היכנס עם אפל',
    registerWithApple: 'הרשמה ב- Apple',
    signInWithGoogle: 'היכנס באמצעות Google',
    registerWithGoogle: 'הרשמה ב- Google',
    forgotYourPassword: 'שכחת ססמה?',
    emailIsNotEnteredCorrectly: 'הדואר האלקטרוני לא הוזן כהלכה',
    enterMoreThanCharacters: 'הזן יותר מ -12 תווים',
    enterMoreThanPasswordCharacters: 'הזן יותר מ -6 תווים',
    passwordDoesNotMatch: 'סיסמה לא מתאימה',
    pleaseEnterPassword: 'אנא הזן סיסמה',
    pleaseEnterYourEmail: 'אנא הכנס את הדוא"ל שלך',
    hide: 'להתחבא',
  ),
  popUpDictionary: PopUpDictionary(
    logOutTitle: 'האם אתה בטוח שברצונך לסגור את היישום',
    logOutNo: 'לא',
    logOutYes: 'כן',
    tutorialTitle: 'אתה יכול למחוק מזון על ידי החלקה ימינה או שמאלה',
    ok: 'בסדר!',
    notificationTitle: '-',
    notificationBody: '-',
    favoritesDelete: 'מתכון זה הוסר מהמועדפים',
    favoritesAdd: 'המתכון הזה התאים למועדפים',
    errorInternetConnection: 'היה באג בשרת, אנא המתן ..',
    alreadyRegistered: 'משתמש כזה כבר רשום',
  ),
  homeDictionary: HomeDictionary(clearAll: 'נקה הכל', watchRecipe: 'צפה במתכון'),
  recipesDictionary: RecipesDictionary(
    yourListIsEmpty: 'הרשימה שלך ריקה',
    youDontHave: 'אין לך:',
  ),
  detailCardsDictionary: DetailCardsDictionary(foodElements: 'רכיבי מזון:', youDontHave: 'אין לך:',
      youHave: 'יש לך:', cooking: 'בישול:', similarRecipes: 'מתכונים דומים'),
);

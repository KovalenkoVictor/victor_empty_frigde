import 'package:base_project_template/dictionary/dictionary_classes/app_bar_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/bottom_navigation_bar_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/detail_cards_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/favorites_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/home_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/on_boarding_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/pop_up_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/recipes_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/settings_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/sign_in_sign_up_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/text_field_dictionary.dart';
import 'package:base_project_template/dictionary/models/language.dart';

Language ru = Language(
  onBoardingPage: OnBoardingDictionary(
      titleTextOneOnBoarding: 'Выберите продукты, которые у вас есть',
      titleTextTwoOnBoarding: 'Выберите все продукты и нажмите "посмотреть рецепты".',
      titleTextThreeOnBoarding: 'Выберите рецепт из предложенного списка.',
      titleTextFourOnBoarding: 'Следуйте рецепту и приятного аппетита',
      buttonTextNext: 'Следующий',
      buttonTextStart: 'Начало!'),
  appBarDictionary: AppBarDictionary(back: 'Назад', favorites: 'Избранное', settings: 'Настройки', notification: 'Уведомление', recipes: 'Рецепты'),
  bottomNavigationBarDictionary: BottomNavigationBarDictionary(home: 'Дом', favorites: 'Избранное', settings: 'Настройки'),
  settingsDictionary: SettingsDictionary(
    logOut: 'Выйти',
    language: 'Язык',
    notification: 'Уведомление',
    aboutCompany: 'О компании',
  ),
  textFieldDictionary: TextFieldDictionary(chooseTheFood: 'Выберите продукты питания', notFound: 'Не найдено'),
  favoritesDictionary: FavoritesDictionary(yourListIsEmpty: 'Ваш список пуст'),
  singInSingUpDictionary: SingInSingUpDictionary(
    signIn: 'Войти',
    signUp: 'Регистрация',
    logIn: 'Авторизоваться',
    email: 'Электронная почта',
    password: 'Пароль',
    confirmPassword: 'Подтвердите пароль',
    show: 'Показать',
    signInWithApple: 'Войти через Apple',
    registerWithApple: 'Регистрация в Apple',
    signInWithGoogle: 'Войти через Google',
    registerWithGoogle: 'Регистрация в Google',
    forgotYourPassword: 'Забыли Ваш пароль?',
    emailIsNotEnteredCorrectly: 'Электронная почта введена неправильно',
    enterMoreThanCharacters: 'Введите более 12 символов',
    enterMoreThanPasswordCharacters: 'Введите более 6 символов',
    passwordDoesNotMatch: 'Пароль не подходит',
    pleaseEnterPassword: 'Пожалуйста введите пароль',
    pleaseEnterYourEmail: 'Пожалуйста, введите ваш адрес электронной почты',
    hide: 'Скрыть',
  ),
  popUpDictionary: PopUpDictionary(
    logOutTitle: 'Вы уверены, что хотите выйти из приложения',
    logOutNo: 'Нет',
    logOutYes: 'Да',
    tutorialTitle: 'Вы можете удалить еду, проведя пальцем вправо или влево',
    ok: 'Хорошо!',
    notificationTitle: '-',
    notificationBody: '-',
    favoritesDelete: 'Этот рецепт был удален из фаворитов',
    favoritesAdd: 'Этот рецепт добавлен в избранное',
    errorInternetConnection: 'На сервере произошла ошибка, подождите ...',
    alreadyRegistered: 'Такой пользователь уже зарегистрирован',
  ),
  homeDictionary: HomeDictionary(clearAll: 'Очистить всё', watchRecipe: 'Посмотреть рецепт'),
  recipesDictionary: RecipesDictionary(yourListIsEmpty: 'Ваш список пуст', youDontHave: 'У вас нет:'),
  detailCardsDictionary: DetailCardsDictionary(foodElements: 'Элементы питания:', youDontHave: 'У вас нет:',
      youHave: 'У вас есть:', cooking: 'Готовка:', similarRecipes: 'Похожие рецепты'),
);

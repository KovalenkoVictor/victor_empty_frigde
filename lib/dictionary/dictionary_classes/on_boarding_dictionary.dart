
class OnBoardingDictionary {
  String titleTextOneOnBoarding;
  String titleTextTwoOnBoarding;
  String titleTextThreeOnBoarding;
  String titleTextFourOnBoarding;
  String buttonTextNext;
  String buttonTextStart;

  OnBoardingDictionary({
    required this.titleTextOneOnBoarding,
    required this.titleTextTwoOnBoarding,
    required this.titleTextThreeOnBoarding,
    required this.titleTextFourOnBoarding,
    required this.buttonTextNext,
    required this.buttonTextStart,
  });
}


class AppBarDictionary {
 String back;
 String favorites;
 String settings;
 String notification;
 String recipes;

  AppBarDictionary({
  required this.back,
  required this.favorites,
  required this.settings,
  required this.notification,
  required this.recipes,
  });
}

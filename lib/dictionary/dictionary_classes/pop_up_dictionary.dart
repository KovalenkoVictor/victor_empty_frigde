class PopUpDictionary {
  String logOutTitle;
  String logOutNo;
  String logOutYes;

  String tutorialTitle;

  String ok;

  String notificationTitle;
  String notificationBody;

  String favoritesDelete;
  String favoritesAdd;
  String errorInternetConnection;
  String alreadyRegistered;

  PopUpDictionary({
    required this.logOutTitle,
    required this.logOutNo,
    required this.logOutYes,
    required this.tutorialTitle,
    required this.ok,
    required this.notificationTitle,
    required this.notificationBody,
    required this.favoritesDelete,
    required this.favoritesAdd,
    required this.errorInternetConnection,
    required this.alreadyRegistered,
  });
}

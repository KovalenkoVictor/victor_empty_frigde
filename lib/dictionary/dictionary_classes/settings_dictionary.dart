
class SettingsDictionary {
  String logOut;
  String language;
  String notification;
  String aboutCompany;

  SettingsDictionary({
    required this.logOut,
    required this.language,
    required this.notification,
    required this.aboutCompany,
  });
}

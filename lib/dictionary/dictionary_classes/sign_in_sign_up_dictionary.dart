class SingInSingUpDictionary {
  String signIn;
  String signUp;
  String logIn;
  String email;
  String password;
  String confirmPassword;
  String show;
  String signInWithGoogle;
  String registerWithGoogle;
  String signInWithApple;
  String registerWithApple;
  String forgotYourPassword;
  String hide;

  String pleaseEnterYourEmail;
  String enterMoreThanCharacters;
  String emailIsNotEnteredCorrectly;

  String pleaseEnterPassword;
  String passwordDoesNotMatch;
  String enterMoreThanPasswordCharacters;

  SingInSingUpDictionary({
    required this.signIn,
    required this.signUp,
    required this.logIn,
    required this.email,
    required this.password,
    required this.confirmPassword,
    required this.show,
    required this.signInWithApple,
    required this.registerWithApple,
    required this.signInWithGoogle,
    required this.registerWithGoogle,
    required this.forgotYourPassword,

    required this.pleaseEnterYourEmail,
    required this.enterMoreThanCharacters,
    required this.emailIsNotEnteredCorrectly,
    required this.pleaseEnterPassword,
    required this.passwordDoesNotMatch,
    required this.enterMoreThanPasswordCharacters,
    required this.hide,
  });
}

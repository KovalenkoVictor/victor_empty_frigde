
class BottomNavigationBarDictionary{
  String home;
  String favorites;
  String settings;
  BottomNavigationBarDictionary({
    required this.home,
    required this.favorites,
    required this.settings,
});
}
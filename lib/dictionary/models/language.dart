import 'package:base_project_template/dictionary/dictionary_classes/app_bar_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/bottom_navigation_bar_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/detail_cards_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/favorites_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/home_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/on_boarding_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/pop_up_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/recipes_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/settings_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/sign_in_sign_up_dictionary.dart';
import 'package:base_project_template/dictionary/dictionary_classes/text_field_dictionary.dart';

class Language {
   OnBoardingDictionary? onBoardingPage;
   AppBarDictionary? appBarDictionary;
   BottomNavigationBarDictionary? bottomNavigationBarDictionary;
   SettingsDictionary? settingsDictionary;
   TextFieldDictionary? textFieldDictionary;
   FavoritesDictionary? favoritesDictionary;
   SingInSingUpDictionary? singInSingUpDictionary;
   PopUpDictionary? popUpDictionary;
   HomeDictionary? homeDictionary;
   RecipesDictionary? recipesDictionary;
   DetailCardsDictionary? detailCardsDictionary;

  Language({
    this.onBoardingPage,
    this.appBarDictionary,
    this.bottomNavigationBarDictionary,
    this.settingsDictionary,
    this.textFieldDictionary,
    this.favoritesDictionary,
    this.singInSingUpDictionary,
    this.popUpDictionary,
    this.homeDictionary,
    this.recipesDictionary,
    this.detailCardsDictionary,
  });
}



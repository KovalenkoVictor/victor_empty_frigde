import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/data/he.dart';
import 'package:base_project_template/dictionary/data/ru.dart';

import 'package:flutter/material.dart';

import 'package:base_project_template/dictionary/models/supported_language.dart';
import 'package:base_project_template/res/locales.dart';

class SupportedLocales {
  List<SupportedLanguage>? _supportedLocales;

  SupportedLocales._() {
    _supportedLocales = <SupportedLanguage>[
      SupportedLanguage(
        languageCode:'en',
        language: en,
      )..choose(),
      SupportedLanguage(
        languageCode:'ru',
        language:ru,
      ),
      SupportedLanguage(
        languageCode:'he',
        language:he,
      ),
    ];
  }

  static SupportedLocales instance = SupportedLocales._();

  void changeLocale(String languageCode) {
    _supportedLocales!.firstWhere((SupportedLanguage supLang) => supLang.isSelected!).discard();
    _supportedLocales!.firstWhere((SupportedLanguage supLang) => supLang.languageCode == languageCode).choose();
  }

  List<Locale> get getSupportedLocales {
    return (_supportedLocales?.map((SupportedLanguage supLang) => supLang.getLocale).toList() ?? <SupportedLanguage>[]) as List<Locale>;
  }

  String get getCurrentLocale {
    return _supportedLocales?.firstWhere((SupportedLanguage supLang) => supLang.isSelected!).languageCode ?? Locales.base;
  }

  SupportedLanguage getSupportedLanguage(Locale locale) {
    return _supportedLocales!.firstWhere((SupportedLanguage supLang) => supLang.languageCode == locale.languageCode);
  }

}
import 'package:base_project_template/services/network_service/dto/recipe_dto/recipe_dto.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/route_service/route_selectors.dart';
import 'package:redux/redux.dart';

class RouteViewModel {
  final Function pop;
  final void Function(String name,[RecipeDto? value]) push;
  final Function pushNamedAndRemoveUntil;

  RouteViewModel({
    required this.pop,
    required this.push,
    required this.pushNamedAndRemoveUntil,
  });

  static RouteViewModel fromStore(Store<AppState> store) {
    return RouteViewModel(
      pop: RouteSelectors.goToBack(store),
      push: RouteSelectors.goPage(store),
      pushNamedAndRemoveUntil: RouteSelectors.pushNamedAndRemoveUntil(store),
    );
  }
}

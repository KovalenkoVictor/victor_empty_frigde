import 'package:base_project_template/res/const_rout.dart';
import 'package:base_project_template/services/network_service/dto/recipe_dto/recipe_dto.dart';
import 'package:base_project_template/ui/pages/detail_cards_page/detail_cards_page.dart';
import 'package:base_project_template/ui/pages/favorites/favorites.dart';
import 'package:base_project_template/ui/pages/home_page/home_page.dart';
import 'package:base_project_template/ui/pages/notification/notification.dart';
import 'package:base_project_template/ui/pages/on_boarding/on_boarding_page.dart';
import 'package:base_project_template/ui/pages/recipes/recipes_page.dart';
import 'package:base_project_template/ui/pages/settings/settings.dart';
import 'package:base_project_template/ui/pages/sign_in_sign_up/sign_in_sign_up_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';

class RouteHelper {
  static String get previousPage => pages.length > 1 ? pages[pages.length - 2] : '';

  static String get currentPage => pages.isNotEmpty ? pages.last :ConstRout.home;

  static List<String> pages = [];

  RouteHelper._privateConstructor();

  static final RouteHelper _instance = RouteHelper._privateConstructor();

  static RouteHelper get instance => _instance;

  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case (ConstRout.home):
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => HomePage(),
        );
      case (ConstRout.favorites):
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => FavoritesPage(),
        );
      case (ConstRout.settings):
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => SettingsPage(),
        );
      case (ConstRout.notification):
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => NotificationPage(),
        );
      case (ConstRout.signInSignUp):
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => SignInSignUpPage(),
        );
      case (ConstRout.onBoarding):
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => OnBoarding(),
        );
      case (ConstRout.recipes):
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => RecipesPage(),
        );
      case (ConstRout.detailCardsPage):
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => DetailCardsPage(recipe:settings.arguments as RecipeDto,),
        );
      default:
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => HomePage(),
        );
    }
  }
  NavigateToAction? canPop() {
    if (pages.isNotEmpty) {
      pages.removeLast();
    }
    print(pages);
    if (pages.isEmpty) {
      return NavigateToAction.push(ConstRout.home);
    }
    return NavigateToAction.pop();
  }


  NavigateToAction? push(String route, {Object? argument}) {
    if(currentPage == ConstRout.detailCardsPage){
      pages.add(route);
      return NavigateToAction.push(route,arguments:argument);
    }

    if (currentPage == route) {
      return null;
    }
    if (previousPage == currentPage) {
      print(currentPage);
      return NavigateToAction.popUntil();
    }
    if (pages.length >= 2) {
      pages.removeWhere((element) => element == route);
    }

    pages.add(route);
    print(pages);
    return NavigateToAction.push(route,arguments:argument);
  }

  NavigateToAction? pushNamedAndRemoveUntil(String route, {Object? argument}) {
      if (currentPage ==ConstRout.home) {
        return null;
      }

    pages.removeRange(0, pages.length);
    print(pages);
    return NavigateToAction.pushNamedAndRemoveUntil(route, (route) => false);
  }
}

import 'package:base_project_template/services/network_service/dto/recipe_dto/recipe_dto.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/route_service/route_helper.dart';
import 'package:redux/redux.dart';

class RouteSelectors {


  static void Function() goToBack(Store<AppState> store) {
    return () => store.dispatch(RouteHelper.instance.canPop());
    }
  static void Function(String routeName,[RecipeDto? value]) goPage(Store<AppState> store) {
    return (name,[value]) => store.dispatch(RouteHelper.instance.push(name,argument:value));
  }
  static void Function(String routeName) pushNamedAndRemoveUntil(Store<AppState> store) {
    return (name) => store.dispatch(RouteHelper.instance.pushNamedAndRemoveUntil(name));
  }

}

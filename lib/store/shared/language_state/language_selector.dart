import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/language_state/language_actions.dart';
import 'package:redux/redux.dart';

class LanguageSelectors {
  static void Function(String routeName) needLanguage(Store<AppState> store) {
    return (name) => store.dispatch(NeedLanguageActions(name),);
  }
}

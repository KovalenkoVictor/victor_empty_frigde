import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/language_state/language_selector.dart';
import 'package:redux/redux.dart';

class LanguageViewModel {
  final void Function(String name) needLanguage;

  LanguageViewModel({
    required this.needLanguage,
  });

  static LanguageViewModel fromStore(Store<AppState> store) {
    return LanguageViewModel(
      needLanguage: LanguageSelectors.needLanguage(store),
    );
  }
}

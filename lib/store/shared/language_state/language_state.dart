import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/store/shared/language_state/language_actions.dart';


class LanguageState {
 final String language;

  LanguageState({
   required this.language
  });
  factory LanguageState.initial() {
    return LanguageState(
      language: 'en',
    );
  }
  LanguageState copyWith({
    String? language,
  }) {
    return LanguageState(
      language: language ?? this.language,
    );
  }
  LanguageState reducer(dynamic action) {
    switch(action.runtimeType){
      case NeedLanguageActions:
        return _newLanguage(action.language);
      default:
        return copyWith();
    }
  }

  LanguageState _newLanguage(String newLanguage) {
    FlutterDictionary.instance.setNewLanguage(newLanguage);
    return copyWith(language:newLanguage);
  }
}
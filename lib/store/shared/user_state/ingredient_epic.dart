import 'package:base_project_template/dictionary/flutter_delegate.dart';
import 'package:base_project_template/services/network_service/dto/ingredient_dto/ingredient_dto.dart';
import 'package:base_project_template/services/network_service/models/base_http_response.dart';
import 'package:base_project_template/services/network_service/models/get_request_model.dart';
import 'package:base_project_template/services/network_service/network_service.dart';
import 'package:base_project_template/services/network_service/res/consts.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/user_state/user_actions.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

class IngredientEpic {
  static final ingredientEpics = combineEpics([getIngredientToken]);

  static Stream<dynamic> getIngredientToken(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<IngredientsAction>().switchMap(
      (action) async* {
        yield RefreshTokenAction();

        final List<IngredientDto> listIngredient = [];
        final BaseHttpResponse<dynamic> ingredientUserService = await NetworkService.instance.request(GetRequestModel(
          url: Uri.parse(
              '$API_KEY/getIngredients?token=${action.ingredientToken}&locale=${FlutterDictionaryDelegate.getCurrentLocale}&str=${action.textController}'),
        ));
        for (var i in ingredientUserService.response) {
          listIngredient.add(IngredientDto.fromJson(i));
        }
        yield SaveIngredientAction(userIngredientDto: listIngredient);
      },
    );
  }
}

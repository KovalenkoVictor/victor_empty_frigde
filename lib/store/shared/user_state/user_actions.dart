import 'package:base_project_template/services/network_service/dto/favorites_dto/favorites_dto.dart';
import 'package:base_project_template/services/network_service/dto/ingredient_dto/ingredient_dto.dart';
import 'package:base_project_template/services/network_service/dto/recipe_dto/recipe_dto.dart';
import 'package:base_project_template/services/network_service/dto/user_dto/dto.dart';
import 'package:base_project_template/services/network_service/dto/user_dto/refresh_token_dto.dart';

class SaveUserAction {
  final Dto userDto;

  SaveUserAction({required this.userDto});
}

class SaveAllIngredientAction {
  final List<IngredientDto> userAllIngredientDto;

  SaveAllIngredientAction({required this.userAllIngredientDto});
}

class SaveMissIngredientAction {
  final List<List<IngredientDto>> userMissIngredientDto;

  SaveMissIngredientAction({required this.userMissIngredientDto});
}

class LogoutUserAction {}

class IngredientsAction {
  String? ingredientToken;
  String? textController;

  IngredientsAction({
    required this.ingredientToken,
    required this.textController,
  });
}

class AddedIngredientsAction {
  final List<IngredientDto> saveIngredient;

  AddedIngredientsAction({
    required this.saveIngredient,
  });
}

class ClearListIngredientAction {}

class SaveIngredientAction {
  final List<IngredientDto> userIngredientDto;

  SaveIngredientAction({required this.userIngredientDto});
}

class RemoveAtIngredientAction {
  int number;

  RemoveAtIngredientAction({
    required this.number,
  });
}

class GetRecipeAction {
  String token;
  List<String> ids;

  GetRecipeAction({
    required this.token,
    required this.ids,
  });
}

class SaveRecipeAction {
  final List<RecipeDto> recipeDto;

  SaveRecipeAction({required this.recipeDto});
}

class GetListFavoritesAction {}

class GetFavoritesAction {
  final List<FavoritesDto> listFavorites;

  GetFavoritesAction({required this.listFavorites});
}

class AddFavoriteAction {
  String token;
  String i;

  AddFavoriteAction({
    required this.token,
    required this.i,
  });
}

class RemoveFavoriteAction {
  String token;
  String i;

  RemoveFavoriteAction({
    required this.token,
    required this.i,
  });
}

class RecipesOrFavorites {
  bool value;

  RecipesOrFavorites({
    required this.value,
  });
}

class RefreshTokenAction {}

class AuthToken {
  RefreshTokenDto authToken;

  AuthToken({
    required this.authToken,
  });
}

class ListIdsAction {
  List<String> ids;

  ListIdsAction({
    required this.ids,
  });
}

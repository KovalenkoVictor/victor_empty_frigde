import 'package:base_project_template/dictionary/flutter_delegate.dart';
import 'package:base_project_template/res/const_rout.dart';
import 'package:base_project_template/services/dialog_service/dialog_service.dart';
import 'package:base_project_template/services/dialog_service/dialogs/loader_dialog/loader_dialog.dart';
import 'package:base_project_template/services/network_service/dto/ingredient_dto/ingredient_dto.dart';
import 'package:base_project_template/services/network_service/dto/recipe_dto/recipe_dto.dart';
import 'package:base_project_template/services/network_service/models/base_http_response.dart';
import 'package:base_project_template/services/network_service/models/get_request_model.dart';
import 'package:base_project_template/services/network_service/network_service.dart';
import 'package:base_project_template/services/network_service/res/consts.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/detail_cards_state/detail_cards_actions.dart';
import 'package:base_project_template/store/shared/loader/loader_state.dart';
import 'package:base_project_template/store/shared/route_service/route_helper.dart';
import 'package:base_project_template/store/shared/user_state/user_actions.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';
import 'package:darq/darq.dart';

class RecipeEpic {
  static final recipeEpics = combineEpics([getRecipeAndAllIngredients]);

  static Stream<dynamic> getRecipeAndAllIngredients(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<GetRecipeAction>().switchMap(
      (action) async* {
        yield RefreshTokenAction();
        final List<RecipeDto> listRecipe = [];
        final List<IngredientDto> listAllIngredient = [];
        final List<List<IngredientDto>> listMissIngredients = [];
        final List<List<IngredientDto>> listHaveIngredients = [];

        DialogService.instance.show(LoaderDialog(state: true, title: '_', loaderKey: LoaderKey.initializationLoading));

        final BaseHttpResponse<dynamic> allIngredientUserService = await NetworkService.instance.request(GetRequestModel(
          url: Uri.parse('$API_KEY/getIngredients?token=${action.token}&locale=${FlutterDictionaryDelegate.getCurrentLocale}&str='),
        ));
        for (var i in allIngredientUserService.response) {
          listAllIngredient.add(IngredientDto.fromJson(i));
        }
        yield SaveAllIngredientAction(userAllIngredientDto: listAllIngredient);

        final BaseHttpResponse<dynamic> recipeUserService = await NetworkService.instance.request(GetRequestModel(
          url: Uri.parse('$API_KEY/getRecipe?token=${action.token}&locale=${FlutterDictionaryDelegate.getCurrentLocale}&ids=${action.ids.join(',')}'),
        ));
        for (var i in recipeUserService.response) {
          listRecipe.add(RecipeDto.fromJson(i));
        }

        final sortList = listRecipe.orderBy((i) => i.missIngredients).toList();
        yield SaveRecipeAction(recipeDto: sortList);
        for (var recipe in sortList) {
          final List<IngredientDto> missIngredients = [];
          final List<IngredientDto> haveIngredients = [];

          for (var ingredient in recipe.ingredients) {
            missIngredients.add(listAllIngredient.where((element) {
              if (element.i == int.parse(ingredient.i)) {
                element.count = ingredient.count;
                element.description = ingredient.description;
              }
              return element.i == int.parse(ingredient.i);
            }).single);
          }

          for (var i in missIngredients) {
            for (var j in action.ids) {
              if (i.i.toString() == j) {
                haveIngredients.add(i);
              }
            }
          }

          for (var id in action.ids) {
            missIngredients.removeWhere((element) {
              return element.i == int.parse(id);
            });
          }

          listHaveIngredients.add(haveIngredients);
          listMissIngredients.add(missIngredients);
        }

        yield SaveMissIngredientAction(userMissIngredientDto: listMissIngredients);
        yield HaveIngredientsAction(haveIngredients: listHaveIngredients);

        DialogService.instance.close();
        yield RecipesOrFavorites(value: true);
        yield RouteHelper.instance.push(ConstRout.recipes);
      },
    );
  }
}

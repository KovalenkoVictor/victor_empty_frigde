import 'dart:collection';
import 'package:base_project_template/services/network_service/dto/favorites_dto/favorites_dto.dart';
import 'package:base_project_template/services/network_service/dto/ingredient_dto/ingredient_dto.dart';
import 'package:base_project_template/services/network_service/dto/recipe_dto/recipe_dto.dart';
import 'package:base_project_template/services/network_service/dto/user_dto/dto.dart';
import 'package:base_project_template/store/shared/detail_cards_state/detail_cards_actions.dart';
import 'package:base_project_template/store/shared/reducer.dart';
import 'package:base_project_template/store/shared/user_state/user_actions.dart';

class UserState {
  final Dto user;
  final List<IngredientDto> ingredient;
  final List<IngredientDto> allIngredient;
  final List<List<IngredientDto>> missIngredient;
  final List<List<IngredientDto>> haveIngredient;
  final List<List<IngredientDto>> listAddedIngredient;
  final List<RecipeDto> recipe;
  final List<FavoritesDto> listFavorite;
  final bool recipesOrFavorites;
  final List<String> ids;


  UserState({
    required this.user,
    required this.ingredient,
    required this.allIngredient,
    required this.missIngredient,
    required this.haveIngredient,
    required this.listAddedIngredient,
    required this.recipe,
    required this.listFavorite,
    required this.recipesOrFavorites,
    required this.ids,
  });

  factory UserState.initial() {
    return UserState(
      user: Dto(createDate: '', refreshToken: '', token: '', ttlToken: ''),
      ingredient: [],
      allIngredient: [],
      missIngredient: [],
      haveIngredient:[],
      listAddedIngredient: [],
      recipe: [],
      listFavorite: [],
      recipesOrFavorites:true,
      ids: [],
    );
  }

  UserState copyWith({
    Dto? value,
    List<IngredientDto>? ingredientValue,
    List<IngredientDto>? allIngredientValue,
    List<List<IngredientDto>>? missIngredientValue,
    List<List<IngredientDto>>? haveIngredientValue,
    List<List<IngredientDto>>? listAddedIngredientValue,
    List<RecipeDto>? recipeValue,
    List<FavoritesDto>? listFavoriteValue,
    bool? recipeOrFavoritesValue,
    List<String>? idsValue,
  }) {
    return UserState(
      user: value ?? user,
      ingredient: ingredientValue ?? ingredient,
      allIngredient: allIngredientValue ?? allIngredient,
      missIngredient: missIngredientValue ?? missIngredient,
      haveIngredient:haveIngredientValue ?? haveIngredient,
      listAddedIngredient: listAddedIngredientValue ?? listAddedIngredient,
      recipe: recipeValue ?? recipe,
      listFavorite:  listFavoriteValue ?? listFavorite,
      recipesOrFavorites: recipeOrFavoritesValue ?? recipesOrFavorites,
      ids: idsValue ?? ids,
    );
  }

  UserState reducer(dynamic action) {
    return Reducer<UserState>(
      actions: HashMap.from({
        SaveUserAction: (action) => _user(action),
        SaveIngredientAction: (action) => _ingredient(action),
        SaveAllIngredientAction: (action) => _saveAllIngredient(action),
        SaveMissIngredientAction: (action) => _saveMissIngredient(action),
        HaveIngredientsAction: (action) => _saveHaveIngredient(action),
        AddedIngredientsAction: (action) => _addedIngredient(action),
        ClearListIngredientAction: (action) => _clearIngredients(),
        RemoveAtIngredientAction: (action) => _removeIngredient(action),
        SaveRecipeAction: (action) => _recipe(action),
        GetFavoritesAction: (action) => _listFavorites(action),
        RecipesOrFavorites: (action) => _recipeOrFavoritesNeedValue(action),
        AuthToken: (action) => _updateToken(action),
        ListIdsAction: (action) => _addIds(action),
      }),
    ).updateState(action, this);
  }

  UserState _user(SaveUserAction action) {
    action.userDto.ttlToken='${DateTime.now().add(Duration(hours: 1))}';
    action.userDto.createDate='${DateTime.now()}';
    return copyWith(value:action.userDto);
  }

  UserState _ingredient(SaveIngredientAction action) {
    return copyWith(ingredientValue: action.userIngredientDto);
  }

  UserState _saveAllIngredient(SaveAllIngredientAction action) {
    return copyWith(allIngredientValue: action.userAllIngredientDto);
  }

  UserState _saveMissIngredient(SaveMissIngredientAction action) {
    return copyWith(missIngredientValue: action.userMissIngredientDto);
  }
  UserState _saveHaveIngredient(HaveIngredientsAction action){
    return copyWith(haveIngredientValue: action.haveIngredients);
  }

  UserState _addedIngredient(AddedIngredientsAction action) {
    listAddedIngredient.add(action.saveIngredient);
    return copyWith(listAddedIngredientValue: listAddedIngredient);
  }

  UserState _clearIngredients() {
    listAddedIngredient.clear();
    return copyWith();
  }

  UserState _removeIngredient(RemoveAtIngredientAction action) {
    listAddedIngredient.removeWhere((element) => element == listAddedIngredient[action.number]);
    return copyWith(listAddedIngredientValue: listAddedIngredient);
  }

  UserState _recipe(SaveRecipeAction action) {
    return copyWith(recipeValue: action.recipeDto);
  }
  UserState _listFavorites(GetFavoritesAction action) {
    return copyWith(listFavoriteValue:action.listFavorites);
  }
  UserState _recipeOrFavoritesNeedValue(RecipesOrFavorites action) {
    return copyWith(recipeOrFavoritesValue:action.value);
  }
  UserState _updateToken(AuthToken action) {
    user.token=action.authToken.authToken;
    user.refreshToken=action.authToken.refreshToken;
    user.ttlToken=action.authToken.ttlToken;
    return copyWith();
  }

  UserState _addIds(ListIdsAction action) {
    return copyWith(idsValue:action.ids);
  }
}

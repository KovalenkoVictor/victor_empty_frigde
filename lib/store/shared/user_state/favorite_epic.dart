import 'package:base_project_template/dictionary/flutter_delegate.dart';
import 'package:base_project_template/res/const_rout.dart';
import 'package:base_project_template/services/dialog_service/dialog_service.dart';
import 'package:base_project_template/services/dialog_service/dialogs/favorites_dialog/favorites_add_dialog/favorites_add_dialog.dart';
import 'package:base_project_template/services/dialog_service/dialogs/favorites_dialog/favorites_delete_dialog/favorites_delete_dialog.dart';
import 'package:base_project_template/services/dialog_service/dialogs/loader_dialog/loader_dialog.dart';
import 'package:base_project_template/services/network_service/dto/favorites_dto/favorites_dto.dart';
import 'package:base_project_template/services/network_service/dto/ingredient_dto/ingredient_dto.dart';
import 'package:base_project_template/services/network_service/models/base_http_response.dart';
import 'package:base_project_template/services/network_service/models/get_request_model.dart';
import 'package:base_project_template/services/network_service/models/post_request_model.dart';
import 'package:base_project_template/services/network_service/network_service.dart';
import 'package:base_project_template/services/network_service/res/consts.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/loader/loader_state.dart';
import 'package:base_project_template/store/shared/route_service/route_helper.dart';
import 'package:base_project_template/store/shared/user_state/user_actions.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';
import 'package:darq/darq.dart';

class FavoriteEpic {
  static final favoriteEpics = combineEpics([
    addFavorite,
    removeFavorite,
    getFavorite,
  ]);

  static Stream<dynamic> getFavorite(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<GetListFavoritesAction>().switchMap((action) async* {
      yield RefreshTokenAction();

      final List<FavoritesDto> listFavorite = [];

      DialogService.instance.show(LoaderDialog(state: true, title: '', loaderKey: LoaderKey.initializationLoading));

      final BaseHttpResponse<dynamic> getFavoriteService = await NetworkService.instance.request(GetRequestModel(
        url: Uri.parse('$API_KEY/getFavorites?token=${store.state.userState!.user.token}&locale=${FlutterDictionaryDelegate.getCurrentLocale}'),
      ));
      for (var i in getFavoriteService.response) {
        listFavorite.add(FavoritesDto.fromJson(i));
      }

      final sortList = listFavorite.orderBy((i) => i.name).toList();
      final List<String> ids = store.state.userState!.ids;

      for (var recipe in sortList) {
        recipe.hasIngredients = recipe.ingredients.count((recipeIngredient) => ids.any((id) => id == recipeIngredient.i));
        recipe.missIngredients = recipe.ingredients.count((recipeIngredient) => !ids.any((id) => id == recipeIngredient.i));
      }

      for (var recipe in sortList) {
        for (var i in recipe.ingredients) {
          for (var element in store.state.userState!.allIngredient) {
            if (element.i == int.parse(i.i)) {
              element.count = i.count;
              element.description = i.description;
            }
          }
        }
      }
       DialogService.instance.close();
      yield GetFavoritesAction(listFavorites: sortList);
    });
  }

  static Stream<dynamic> addFavorite(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<AddFavoriteAction>().switchMap(
      (action) async* {
        await NetworkService.instance
            .request(PostRequestModel(url: Uri.parse('$API_KEY/addFavorite?token=${store.state.userState!.user.token}&i=${action.i}')));
        yield GetListFavoritesAction();
        DialogService.instance.show(FavoritesAddDialog());
      },
    );
  }

  static Stream<dynamic> removeFavorite(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<RemoveFavoriteAction>().switchMap(
      (action) async* {
        await NetworkService.instance
            .request(PostRequestModel(url: Uri.parse('$API_KEY/removeFavorite?token=${store.state.userState!.user.token}&i=${action.i}')));
        yield GetListFavoritesAction();
        DialogService.instance.show(FavoritesDeleteDialog());
      },
    );
  }
}

import 'package:base_project_template/services/network_service/dto/favorites_dto/favorites_dto.dart';
import 'package:base_project_template/services/network_service/dto/ingredient_dto/ingredient_dto.dart';
import 'package:base_project_template/services/network_service/dto/recipe_dto/recipe_dto.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/user_state/user_actions.dart';
import 'package:redux/redux.dart';

class UserSelector {
  static String getUserToken(Store<AppState> store) {
    return store.state.userState!.user.token;
  }

  static String getUserCreateDate(Store<AppState> store) {
    return store.state.userState!.user.createDate;
  }

  static String getUserRefreshToken(Store<AppState> store) {
    return store.state.userState!.user.refreshToken;
  }

  static String getUserTtlToken(Store<AppState> store) {
    return store.state.userState!.user.ttlToken;
  }

  static Function getLogout(Store<AppState> store) {
    return () => store.dispatch(LogoutUserAction());
  }

  static List<IngredientDto> getValueIngredients(Store<AppState> store) {
    return store.state.userState!.ingredient;
  }

  static List<IngredientDto> getAllIngredients(Store<AppState> store) {
    return store.state.userState!.allIngredient;
  }

  static List<List<IngredientDto>> getMissIngredients(Store<AppState> store) {
    return store.state.userState!.missIngredient;
  }
  static List<List<IngredientDto>> getHaveIngredients(Store<AppState> store) {
    return store.state.userState!.haveIngredient;
  }

  static Function(String? token, String? textController) getIngredients(Store<AppState> store) {
    return (token, textController) => store.dispatch(IngredientsAction(ingredientToken: token, textController: textController));
  }

  static Function(List<IngredientDto> saveIngredient) getSaveIngredients(Store<AppState> store) {
    return (saveIngredient) => store.dispatch(AddedIngredientsAction(saveIngredient: saveIngredient));
  }

  static List<List<IngredientDto>> getIngredientsList(Store<AppState> store) {
    return store.state.userState!.listAddedIngredient;
  }

  static Function() getClearIngredients(Store<AppState> store) {
    return () => store.dispatch(ClearListIngredientAction());
  }

  static Function(int value) removeIngredient(Store<AppState> store) {
    return (value) => store.dispatch(RemoveAtIngredientAction(number: value));
  }

  static Function(String token, List<String> ids) getRecipe(Store<AppState> store) {
    return (token, ids) => store.dispatch(GetRecipeAction(token: token, ids: ids));
  }

  static List<RecipeDto> getValueRecipe(Store<AppState> store) {
    return store.state.userState!.recipe;
  }

  static Function(String token,String i) addFavorite(Store<AppState> store) {
    return (token,i) => store.dispatch(AddFavoriteAction(token: token, i: i));
  }
  static Function(String token,String i) removeFavorite(Store<AppState> store) {
    return (token,i) => store.dispatch(RemoveFavoriteAction(token: token, i: i));
  }
  static List<FavoritesDto> getListFavorites(Store<AppState> store) {
    return store.state.userState!.listFavorite;
  }

  static bool getRecipeOrFavoritesValue(Store<AppState> store) {
    return store.state.userState!.recipesOrFavorites;
  }
  static Function(bool value) getRecipeOrFavoritesNeedValue(Store<AppState> store) {
    return (value) => store.dispatch(RecipesOrFavorites(value:value));
  }

  static Function() getListFavoritesVm(Store<AppState> store) {
    return () => store.dispatch(GetListFavoritesAction());
  }

  static Function(List<String> value) addListIds(Store<AppState> store) {
    return (value) => store.dispatch(ListIdsAction(ids: value));
  }

  static List<String> getListIds(Store<AppState> store) {
    return store.state.userState!.ids;
  }
}

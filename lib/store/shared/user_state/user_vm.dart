import 'package:base_project_template/services/network_service/dto/favorites_dto/favorites_dto.dart';
import 'package:base_project_template/services/network_service/dto/ingredient_dto/ingredient_dto.dart';
import 'package:base_project_template/services/network_service/dto/recipe_dto/recipe_dto.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/user_state/user_selector.dart';
import 'package:redux/redux.dart';

class UserViewModel {
  final String token;
  final String refreshToken;
  final String ttlToken;
  final String createDate;

  final Function logout;
  final Function(String? token, String? textController) ingredientsToken;

  final List<IngredientDto> ingredients;
  final List<IngredientDto> allIngredients;
  final List<List<IngredientDto>> missIngredients;
  final List<List<IngredientDto>> haveIngredients;

  final List<List<IngredientDto>> listAddedIngredients;
  final Function(List<IngredientDto> saveIngredient) addedIngredients;
  final Function() clearIngredients;
  final Function(int value) removeIngredient;
  final Function(String token, List<String> ids) getRecipe;

  final List<RecipeDto> recipe;
  final List<FavoritesDto> listFavorites;

  final Function(String token,String i) addFavorite;
  final Function(String token,String i) removeFavorite;

  final bool recipeOrFavorites;
  final Function(bool value) recipeOrFavoritesNeedValue;

  final Function() getListFavorites;

  final Function(List<String> value) addIds;
  final List<String> ids;

  UserViewModel({
    required this.token,
    required this.refreshToken,
    required this.ttlToken,
    required this.createDate,
    required this.logout,
    required this.ingredientsToken,
    required this.ingredients,
    required this.allIngredients,
    required this.missIngredients,
    required this.haveIngredients,
    required this.addedIngredients,
    required this.listAddedIngredients,
    required this.clearIngredients,
    required this.removeIngredient,
    required this.getRecipe,
    required this.recipe,
    required this.addFavorite,
    required this.removeFavorite,
    required this.listFavorites,
    required this.recipeOrFavorites,
    required this.recipeOrFavoritesNeedValue,
    required this.getListFavorites,
    required this.addIds,
    required this.ids,
  });

  static UserViewModel fromStore(Store<AppState> store) {
    return UserViewModel(
      token: UserSelector.getUserToken(store),
      refreshToken: UserSelector.getUserRefreshToken(store),
      ttlToken: UserSelector.getUserTtlToken(store),
      createDate: UserSelector.getUserCreateDate(store),
      logout: UserSelector.getLogout(store),
      ingredientsToken: UserSelector.getIngredients(store),
      ingredients: UserSelector.getValueIngredients(store),
      allIngredients: UserSelector.getAllIngredients(store),
      missIngredients: UserSelector.getMissIngredients(store),
      haveIngredients: UserSelector.getHaveIngredients(store),
      addedIngredients: UserSelector.getSaveIngredients(store),
      listAddedIngredients: UserSelector.getIngredientsList(store),
      clearIngredients: UserSelector.getClearIngredients(store),
      removeIngredient: UserSelector.removeIngredient(store),
      getRecipe: UserSelector.getRecipe(store),
      recipe: UserSelector.getValueRecipe(store),
      addFavorite:UserSelector.addFavorite(store),
      removeFavorite:UserSelector.removeFavorite(store),
      listFavorites: UserSelector.getListFavorites(store),
      recipeOrFavorites: UserSelector.getRecipeOrFavoritesValue(store),
      recipeOrFavoritesNeedValue: UserSelector.getRecipeOrFavoritesNeedValue(store),
      getListFavorites: UserSelector.getListFavoritesVm(store),
      addIds: UserSelector.addListIds(store),
      ids: UserSelector.getListIds(store),
    );
  }
}

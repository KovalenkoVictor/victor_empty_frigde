import 'package:base_project_template/res/const.dart';
import 'package:base_project_template/services/dialog_service/dialog_service.dart';
import 'package:base_project_template/services/dialog_service/dialogs/loader_dialog/loader_dialog.dart';
import 'package:base_project_template/services/network_service/dto/user_dto/refresh_token_dto.dart';
import 'package:base_project_template/services/network_service/models/base_http_response.dart';
import 'package:base_project_template/services/network_service/models/get_request_model.dart';
import 'package:base_project_template/services/network_service/network_service.dart';
import 'package:base_project_template/services/network_service/res/consts.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/loader/loader_state.dart';
import 'package:base_project_template/store/shared/user_state/user_actions.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

class RefreshTokenEpic {
  static final refreshTokenEpics = combineEpics([getRefreshToken]);

  static Stream<dynamic> getRefreshToken(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<RefreshTokenAction>().switchMap(
      (action) async* {
        if (DateTime.now().toUtc().isAfter(DateTime.parse(store.state.userState!.user.ttlToken))) {
          final BaseHttpResponse<dynamic> refreshService = await NetworkService.instance
              .request(GetRequestModel(url: Uri.parse('$API_KEY/updateToken?refreshToken=${store.state.userState!.user.refreshToken}')));
          if (refreshService.error?.error == NOT_FOUND) {
            DialogService.instance.show(LoaderDialog(state: true, title: '_', loaderKey: LoaderKey.initializationLoading));
          } else {
            yield AuthToken(authToken: RefreshTokenDto.fromJson(refreshService.response));
          }
        }
      },
    );
  }
}

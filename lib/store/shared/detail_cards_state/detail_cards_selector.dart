import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/detail_cards_state/detail_cards_actions.dart';
import 'package:redux/redux.dart';

class DetailCardsSelector{

  static int getIndexCard(Store<AppState> store) {
    return store.state.detailCardsState!.index;
  }
  static void Function(int index) getValueNeedCard(Store<AppState> store) {
    return (index) => store.dispatch(ValueRecipeInCardAction(index: index));
  }
}

import 'package:base_project_template/services/network_service/dto/ingredient_dto/ingredient_dto.dart';

class ValueRecipeInCardAction {
  int index;
  ValueRecipeInCardAction({
    required this.index,
});
}
class HaveIngredientsAction{
  final List<List<IngredientDto>> haveIngredients;
  HaveIngredientsAction({
    required this.haveIngredients,
});
}
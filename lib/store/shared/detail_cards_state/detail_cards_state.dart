import 'package:base_project_template/store/shared/detail_cards_state/detail_cards_actions.dart';

class DetailCardsState {
  final int index;

  DetailCardsState({
    required this.index,
  });

  factory DetailCardsState.initial() {
    return DetailCardsState(
      index: 0,
    );
  }

  DetailCardsState copyWith({
    int? index,
  }) {
    return DetailCardsState(
      index: index ?? this.index,
    );
  }

  DetailCardsState reducer(dynamic action) {
    switch (action.runtimeType) {
      case ValueRecipeInCardAction:
        return _indexCard(action);

      default:
        return copyWith();
    }
  }

  DetailCardsState _indexCard(ValueRecipeInCardAction action) {
    return copyWith(index: action.index);
  }
}

import 'package:base_project_template/dictionary/flutter_delegate.dart';
import 'package:base_project_template/res/const.dart';
import 'package:base_project_template/res/const_rout.dart';
import 'package:base_project_template/services/dialog_service/dialog_service.dart';
import 'package:base_project_template/services/dialog_service/dialogs/already_registered/already_registered_dialog.dart';
import 'package:base_project_template/services/network_service/dto/ingredient_dto/ingredient_dto.dart';
import 'package:base_project_template/services/network_service/dto/user_dto/dto.dart';
import 'package:base_project_template/services/network_service/models/base_http_response.dart';
import 'package:base_project_template/services/network_service/models/get_request_model.dart';
import 'package:base_project_template/services/network_service/models/post_request_model.dart';
import 'package:base_project_template/services/network_service/network_service.dart';
import 'package:base_project_template/services/network_service/res/consts.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/services/dialog_service/dialogs/loader_dialog/loader_dialog.dart';
import 'package:base_project_template/store/shared/loader/loader_state.dart';
import 'package:base_project_template/store/shared/route_service/route_helper.dart';
import 'package:base_project_template/store/shared/sign_in_sign_up_state/sign_in_sign_up_actions.dart';
import 'package:base_project_template/store/shared/user_state/user_actions.dart';
import 'package:rxdart/rxdart.dart';
import 'package:redux_epics/redux_epics.dart';

class LoginAndRegistrationEpic {
  static final loginRegistrationLogoutEpics = combineEpics([getRegistrationServiceState, getLoginServiceState, getLogoutServiceState]);

  static Stream<dynamic> getRegistrationServiceState(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<RegistrationActions>().switchMap(
      (action) async* {
        DialogService.instance.show(LoaderDialog(state: true, title: '', loaderKey: LoaderKey.initializationLoading));

        final BaseHttpResponse<dynamic> registrationUserService = await NetworkService.instance
            .request(GetRequestModel(url: Uri.parse('$API_KEY/registration?firstName=_&email=${action.email}&password=${action.password}')));

        if (registrationUserService.error?.error == FORBIDDEN) {
          DialogService.instance.close();
          DialogService.instance.show(AlreadyRegisteredDialog());
        } else {
          yield LoginActions(email: action.email, password: action.password);
        }
      },
    );
  }

  static Stream<dynamic> getLoginServiceState(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<LoginActions>().switchMap(
      (action) async* {
        final List<IngredientDto> listAllIngredient = [];
        DialogService.instance.show(LoaderDialog(state: true, title: '_', loaderKey: LoaderKey.initializationLoading));

        final BaseHttpResponse<dynamic> loginUserService = await NetworkService.instance
            .request(GetRequestModel(url: Uri.parse('$API_KEY/login?email=${action.email}&password=${action.password}')));

        if (loginUserService.error?.error == NOT_FOUND) {
          DialogService.instance.close();
        } else {
          yield SaveUserAction(userDto: Dto.fromJson(loginUserService.response));

          final BaseHttpResponse<dynamic> allIngredientUserService = await NetworkService.instance.request(GetRequestModel(
            url: Uri.parse(
                '$API_KEY/getIngredients?token=${store.state.userState!.user.token}&locale=${FlutterDictionaryDelegate.getCurrentLocale}&str='),
          ));
          for (var i in allIngredientUserService.response) {
            listAllIngredient.add(IngredientDto.fromJson(i));
          }

          yield SaveAllIngredientAction(userAllIngredientDto: listAllIngredient);

          DialogService.instance.close();

          print('=======${store.state.userState!.user.ttlToken}==========');
          print('=======${store.state.userState!.user.createDate}==========');

          yield RouteHelper.instance.push(ConstRout.onBoarding);
        }
      },
    );
  }

  static Stream<dynamic> getLogoutServiceState(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<LogoutUserAction>().switchMap(
      (action) async* {
        yield RefreshTokenAction();
        DialogService.instance.close();

        DialogService.instance.show(LoaderDialog(state: true, title: '', loaderKey: LoaderKey.initializationLoading));

        await NetworkService.instance.request(PostRequestModel(url: Uri.parse('$API_KEY/logout?token=${store.state.userState!.user.token}')));

        yield RouteHelper.instance.pushNamedAndRemoveUntil(ConstRout.signInSignUp);
      },
    );
  }
}

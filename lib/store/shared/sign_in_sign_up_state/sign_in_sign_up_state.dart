import 'package:base_project_template/store/shared/sign_in_sign_up_state/sign_in_sign_up_actions.dart';

class SignInSignUpState {
  bool signInSignUp;

  bool oneShowValue;
  bool twoShowValue;
  bool threeShowValue;
  bool fourShowValue;

  SignInSignUpState({
    required this.signInSignUp,
    required this.oneShowValue,
    required this.twoShowValue,
    required this.threeShowValue,
    required this.fourShowValue,
  });

  factory SignInSignUpState.initial() {
    return SignInSignUpState(
      signInSignUp: true,
      oneShowValue: true,
      twoShowValue: true,
      threeShowValue: true,
      fourShowValue: true,
    );
  }

  SignInSignUpState copyWith({
    bool? signInSignUp,
    bool? oneShowValue,
    bool? twoShowValue,
    bool? threeShowValue,
    bool? fourShowValue,
  }) {
    return SignInSignUpState(
      signInSignUp: signInSignUp ?? this.signInSignUp,
      oneShowValue: oneShowValue ?? this.oneShowValue,
      twoShowValue: twoShowValue ?? this.twoShowValue,
      threeShowValue: threeShowValue ?? this.threeShowValue,
      fourShowValue: fourShowValue ?? this.fourShowValue,
    );
  }

  SignInSignUpState reducer(dynamic action) {
    switch (action.runtimeType) {
      case ChangeTheValueAction:
        return _signInSignUp();
      case OneShowValueActions:
        return _showOneValue();
      case TwoShowValueActions:
        return _showTwoValue();
      case ThreeShowValueActions:
        return _showThreeValue();
      case FourShowValueActions:
        return _showFourValue();

      default:
        return copyWith();
    }
  }

  SignInSignUpState _signInSignUp() {
    signInSignUp = !signInSignUp;
    return copyWith(signInSignUp: signInSignUp);
  }

  SignInSignUpState _showOneValue() {
    oneShowValue = !oneShowValue;
    return copyWith(oneShowValue: oneShowValue);
  }

  SignInSignUpState _showTwoValue() {
    twoShowValue = !twoShowValue;
    return copyWith(twoShowValue: twoShowValue);
  }

  SignInSignUpState _showThreeValue() {
    threeShowValue = !threeShowValue;
    return copyWith(twoShowValue: threeShowValue);
  }

  SignInSignUpState _showFourValue() {
    fourShowValue = !fourShowValue;
    return copyWith(twoShowValue: fourShowValue);
  }
}

import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/sign_in_sign_up_state/sign_in_sign_up_selector.dart';
import 'package:redux/redux.dart';

class SignInSignUpViewModel {
  bool needValue;
  Function changeValue;

  bool needOneShowValue;
  Function changeOneShowValue;

  bool needTwoShowValue;
  Function changeTwoShowValue;

  bool needThreeShowValue;
  Function changeThreeShowValue;

  bool needFourShowValue;
  Function changeFourShowValue;

  Function(String email, String password) login;
  Function(String email, String password, String confirmPassword) register;

  SignInSignUpViewModel({
    required this.needValue,
    required this.changeValue,
    required this.needOneShowValue,
    required this.changeOneShowValue,
    required this.needTwoShowValue,
    required this.changeTwoShowValue,
    required this.login,
    required this.register,
    required this.needThreeShowValue,
    required this.changeThreeShowValue,
    required this.needFourShowValue,
    required this.changeFourShowValue,
  });

  static SignInSignUpViewModel fromStore(Store<AppState> store) {
    return SignInSignUpViewModel(
      needValue: SignInSignUpSelector.getValue(store),
      changeValue: SignInSignUpSelector.changeValue(store),

      needOneShowValue: SignInSignUpSelector.getOneShowValue(store),
      changeOneShowValue: SignInSignUpSelector.changeOneShowValue(store),

      needTwoShowValue: SignInSignUpSelector.getTwoShowValue(store),
      changeTwoShowValue: SignInSignUpSelector.changeTwoShowValue(store),

      needThreeShowValue: SignInSignUpSelector.getThreeShowValue(store),
      changeThreeShowValue: SignInSignUpSelector.changeThreeShowValue(store),

      changeFourShowValue:  SignInSignUpSelector.changeFourShowValue(store),
      needFourShowValue: SignInSignUpSelector.getFourShowValue(store),


      login: SignInSignUpSelector.getLogin(store),
      register: SignInSignUpSelector.getRegistration(store),
    );
  }
}

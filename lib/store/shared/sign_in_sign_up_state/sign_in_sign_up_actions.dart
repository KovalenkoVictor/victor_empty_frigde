class ChangeTheValueAction {}

class OneShowValueActions {}

class TwoShowValueActions {}

class ThreeShowValueActions {}

class FourShowValueActions {}

class LoginActions {
  String email;
  String password;

  LoginActions({
    required this.email,
    required this.password,
  });
}

class RegistrationActions {
  String email;
  String password;
  String confirmPassword;

  RegistrationActions({
    required this.email,
    required this.password,
    required this.confirmPassword,
  });
}

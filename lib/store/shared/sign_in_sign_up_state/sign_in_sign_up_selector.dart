import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/sign_in_sign_up_state/sign_in_sign_up_actions.dart';
import 'package:redux/redux.dart';

abstract class SignInSignUpSelector {
  static bool getValue(Store<AppState> store) {
    return store.state.signInSignUpState.signInSignUp;
  }

  static Function changeValue(Store<AppState> store) {
    return () => store.dispatch(ChangeTheValueAction());
  }

  static bool getOneShowValue(Store<AppState> store) {
    return store.state.signInSignUpState.oneShowValue;
  }

  static Function changeOneShowValue(Store<AppState> store) {
    return () => store.dispatch(OneShowValueActions());
  }

  static bool getTwoShowValue(Store<AppState> store) {
    return store.state.signInSignUpState.twoShowValue;
  }

  static Function changeTwoShowValue(Store<AppState> store) {
    return () => store.dispatch(TwoShowValueActions());
  }

  static Function(String email, String password) getLogin(Store<AppState> store) {
    return (String email, String password) => store.dispatch(LoginActions(email: email, password: password));
  }

  static Function(String email, String password, String confirmPassword) getRegistration(Store<AppState> store) {
    return (String email, String password, String confirmPassword) =>
        store.dispatch(RegistrationActions(email: email, password: password, confirmPassword: confirmPassword));
  }


  static bool getThreeShowValue(Store<AppState> store) {
    return store.state.signInSignUpState.threeShowValue;
  }

  static Function changeThreeShowValue(Store<AppState> store) {
    return () => store.dispatch(ThreeShowValueActions());
  }
  static bool getFourShowValue(Store<AppState> store) {
    return store.state.signInSignUpState.fourShowValue;
  }

  static Function changeFourShowValue(Store<AppState> store) {
    return () => store.dispatch(FourShowValueActions());
  }
}

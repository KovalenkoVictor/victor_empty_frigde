import 'package:base_project_template/store/shared/detail_cards_state/detail_cards_state.dart';
import 'package:base_project_template/store/shared/language_state/language_state.dart';
import 'package:base_project_template/store/shared/route_service/route_vm.dart';
import 'package:base_project_template/store/shared/sign_in_sign_up_state/sign_in_sign_up_epics.dart';
import 'package:base_project_template/store/shared/sign_in_sign_up_state/sign_in_sign_up_state.dart';
import 'package:base_project_template/store/shared/user_state/favorite_epic.dart';
import 'package:base_project_template/store/shared/user_state/ingredient_epic.dart';
import 'package:base_project_template/store/shared/user_state/recipe_epic.dart';
import 'package:base_project_template/store/shared/user_state/refresh_token_epic.dart';
import 'package:base_project_template/store/shared/user_state/user_state.dart';
import 'package:redux_epics/redux_epics.dart';

class AppState {
  final LanguageState languageState;
  final RouteViewModel? route;
  final SignInSignUpState signInSignUpState;
  final UserState? userState;
  final DetailCardsState? detailCardsState;

  AppState({
    required this.languageState,
    required this.signInSignUpState,
    this.userState,
    this.route,
    this.detailCardsState,
  });

  factory AppState.initial() {
    return AppState(
      languageState: LanguageState.initial(),
      signInSignUpState: SignInSignUpState.initial(),
      userState: UserState.initial(),
      detailCardsState: DetailCardsState.initial(),
    );
  }

  static AppState getReducer(AppState state, dynamic action) {
    return AppState(
      languageState: state.languageState.reducer(action),
      signInSignUpState: state.signInSignUpState.reducer(action),
      userState: state.userState!.reducer(action),
      detailCardsState: state.detailCardsState!.reducer(action),
    );
  }

  static final getAppEpic = combineEpics<AppState>(
      [LoginAndRegistrationEpic.loginRegistrationLogoutEpics, IngredientEpic.ingredientEpics, RecipeEpic.recipeEpics, FavoriteEpic.favoriteEpics,RefreshTokenEpic.refreshTokenEpics]);
}

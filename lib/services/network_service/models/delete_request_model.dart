import 'package:http/http.dart' as http;
import 'package:http/src/response.dart';
import 'package:base_project_template/services/network_service/interfaces/i_base_request.dart';

class DeleteRequestModel implements IBaseRequest {
  final Uri? url;
  final Map<String, String>? headers;

  DeleteRequestModel({
    this.url,
    this.headers,
  });

  @override
  Future<Response> call() {
    return http.delete(
      url!,
      headers: headers,
    );
  }
}

import 'package:http/http.dart' as http;
import 'package:http/src/response.dart';
import 'package:base_project_template/services/network_service/interfaces/i_base_request.dart';

class PutRequestModel implements IBaseRequest {
  final Uri? url;
  final Map<String, String>? headers;
  final Map<String, String>? body;

  PutRequestModel({
    this.url,
    this.headers,
    this.body,
  });

  @override
  Future<Response> call() {
    return http.put(
      url!,
      headers: headers,
      body: body,
    );
  }
}

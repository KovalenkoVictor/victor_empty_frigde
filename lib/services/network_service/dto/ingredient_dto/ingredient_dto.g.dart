// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ingredient_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IngredientDto _$IngredientDtoFromJson(Map<String, dynamic> json) {
  return IngredientDto(
    i: json['i'] as int,
    name: json['name'] as String,
    image: json['image'] as String,
  )
    ..count = json['count'] as String?
    ..description = json['description'] as String?;
}

Map<String, dynamic> _$IngredientDtoToJson(IngredientDto instance) =>
    <String, dynamic>{
      'i': instance.i,
      'name': instance.name,
      'image': instance.image,
      'count': instance.count,
      'description': instance.description,
    };

import 'package:json_annotation/json_annotation.dart';
part 'ingredient_dto.g.dart';

@JsonSerializable()
class IngredientDto {
  int i;
  String name;
  String image;

  String? count;
  String? description;

  IngredientDto({
    required this.i,
    required this.name,
    required this.image,
  });

  factory IngredientDto.fromJson(Map<String, dynamic> json) => _$IngredientDtoFromJson(json);

  Map<String, dynamic> toJson() => _$IngredientDtoToJson(this);
}

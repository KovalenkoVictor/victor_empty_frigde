import 'package:json_annotation/json_annotation.dart';

part 'dto.g.dart';

@JsonSerializable()
class Dto {
  String token;
  String refreshToken;
  String ttlToken;
  String createDate;

  Dto({
    required this.token,
    required this.refreshToken,
    required this.ttlToken,
    required this.createDate,
  });

  factory Dto.fromJson(Map<String, dynamic> json) => _$DtoFromJson(json);

  Map<String, dynamic> toJson() => _$DtoToJson(this);
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'refresh_token_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RefreshTokenDto _$RefreshTokenDtoFromJson(Map<String, dynamic> json) {
  return RefreshTokenDto(
    authToken: json['authToken'] as String,
    refreshToken: json['refreshToken'] as String,
    ttlToken: json['ttlToken'] as String,
  );
}

Map<String, dynamic> _$RefreshTokenDtoToJson(RefreshTokenDto instance) =>
    <String, dynamic>{
      'authToken': instance.authToken,
      'refreshToken': instance.refreshToken,
      'ttlToken': instance.ttlToken,
    };

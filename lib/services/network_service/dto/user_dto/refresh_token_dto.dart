import 'package:json_annotation/json_annotation.dart';

part 'refresh_token_dto.g.dart';

@JsonSerializable()
class RefreshTokenDto {
  String authToken;
  String refreshToken;
  String ttlToken;


  RefreshTokenDto({
    required this.authToken,
    required this.refreshToken,
    required this.ttlToken,
  });

  factory RefreshTokenDto.fromJson(Map<String, dynamic> json) => _$RefreshTokenDtoFromJson(json);

  Map<String, dynamic> toJson() => _$RefreshTokenDtoToJson(this);
}
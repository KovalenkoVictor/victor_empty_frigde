// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Dto _$DtoFromJson(Map<String, dynamic> json) {
  return Dto(
    token: json['token'] as String,
    refreshToken: json['refreshToken'] as String,
    ttlToken: json['ttlToken'] as String,
    createDate: json['createDate'] as String,
  );
}

Map<String, dynamic> _$DtoToJson(Dto instance) => <String, dynamic>{
      'token': instance.token,
      'refreshToken': instance.refreshToken,
      'ttlToken': instance.ttlToken,
      'createDate': instance.createDate,
    };

import 'package:base_project_template/services/network_service/dto/recipe_dto/ingredients_in_recipe/ingredients_in_recipe.dart';
import 'package:json_annotation/json_annotation.dart';

part 'recipe_dto.g.dart';

@JsonSerializable()
class RecipeDto {
  int i;
  String name;
  String image;
  int time;
  double calories;
  String level;
  List<IngredientsInRecipeDto> ingredients;
  List<String> steps;
  int hasIngredients;
  int missIngredients;

  RecipeDto({
    required this.i,
    required this.name,
    required this.image,
    required this.time,
    required this.calories,
    required this.level,
    required this.ingredients,
    required this.steps,
    required this.hasIngredients,
    required this.missIngredients,
  });

  factory RecipeDto.fromJson(Map<String, dynamic> json) => _$RecipeDtoFromJson(json);

  Map<String, dynamic> toJson() => _$RecipeDtoToJson(this);
}

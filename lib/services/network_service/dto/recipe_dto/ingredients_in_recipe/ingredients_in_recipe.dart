import 'package:json_annotation/json_annotation.dart';
part 'ingredients_in_recipe.g.dart';

@JsonSerializable()
class IngredientsInRecipeDto {
  String i;
  String count;
  String? description;

  IngredientsInRecipeDto({
    required this.i,
    required this.count,
    required this.description,
  });

  factory IngredientsInRecipeDto.fromJson(Map<String, dynamic> json) => _$IngredientsInRecipeDtoFromJson(json);

  Map<String, dynamic> toJson() => _$IngredientsInRecipeDtoToJson(this);
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ingredients_in_recipe.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IngredientsInRecipeDto _$IngredientsInRecipeDtoFromJson(
    Map<String, dynamic> json) {
  return IngredientsInRecipeDto(
    i: json['i'] as String,
    count: json['count'] as String,
    description: json['description'] as String?,
  );
}

Map<String, dynamic> _$IngredientsInRecipeDtoToJson(
        IngredientsInRecipeDto instance) =>
    <String, dynamic>{
      'i': instance.i,
      'count': instance.count,
      'description': instance.description,
    };

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favorites_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FavoritesDto _$FavoritesDtoFromJson(Map<String, dynamic> json) {
  return FavoritesDto(
    i: json['i'] as int,
    name: json['name'] as String,
    image: json['image'] as String,
    time: json['time'] as int,
    calories: (json['calories'] as num).toDouble(),
    level: json['level'] as String,
    ingredients: (json['ingredients'] as List<dynamic>)
        .map((e) => IngredientsInRecipeDto.fromJson(e as Map<String, dynamic>))
        .toList(),
    steps: (json['steps'] as List<dynamic>).map((e) => e as String).toList(),
    hasIngredients: json['hasIngredients'] as int,
    missIngredients: json['missIngredients'] as int,
  );
}

Map<String, dynamic> _$FavoritesDtoToJson(FavoritesDto instance) =>
    <String, dynamic>{
      'i': instance.i,
      'name': instance.name,
      'image': instance.image,
      'time': instance.time,
      'calories': instance.calories,
      'level': instance.level,
      'ingredients': instance.ingredients,
      'steps': instance.steps,
      'hasIngredients': instance.hasIngredients,
      'missIngredients': instance.missIngredients,
    };

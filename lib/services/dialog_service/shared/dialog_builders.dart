import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:flutter/material.dart';

class DialogBuilders {
  static Future<void> defaultDialogBuilder({required BuildContext context, Widget? widget}) {
    return showDialog(
      barrierColor: AppColors.transparent,
      context: context,
      builder: (BuildContext ctx) => widget!,
    );
  }

  static Future<void> modalBottomSheetBuilder({required BuildContext context, Widget? widget}) {
    return showModalBottomSheet<dynamic>(
      context: context,
      elevation: 0.0,
      clipBehavior: Clip.hardEdge,
      builder: (BuildContext ctx) => widget!,
    );
  }
}

import 'package:base_project_template/services/dialog_service/dialogs/already_registered/already_registered_widget.dart';
import 'package:base_project_template/services/dialog_service/shared/dialog_builders.dart';
import 'package:base_project_template/services/dialog_service/shared/i_dialog.dart';
import 'package:flutter/material.dart';



class AlreadyRegisteredDialog implements IDialog {
  Future<void> _builder(BuildContext context) {
    return DialogBuilders.defaultDialogBuilder(
      context: context,
      widget: AlreadyRegisteredWidget(),
    );
  }

  @override
  void show(DisplayFunction display) => display(_builder);
}

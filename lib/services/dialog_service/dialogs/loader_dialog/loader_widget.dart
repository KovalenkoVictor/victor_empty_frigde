import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/ui/layouts/loader/loader.dart';
import 'package:base_project_template/ui/layouts/loader_layout/dialog_layout.dart';
import 'package:flutter/material.dart';

class LoaderWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DialogLayout(
      child: Material(
        color: AppColors.kBlack.withOpacity(0.7),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Loader(width:70, height: 70),
             const SizedBox(height: 16.0),
              Text(
                'loading...',
                style: FontStyles.textRegular.copyWith(color: AppColors.white, fontSize: 16.0),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

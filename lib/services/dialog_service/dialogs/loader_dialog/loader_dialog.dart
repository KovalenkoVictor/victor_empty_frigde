import 'package:base_project_template/services/dialog_service/shared/dialog_builders.dart';
import 'package:base_project_template/services/dialog_service/shared/i_dialog.dart';
import 'package:base_project_template/services/dialog_service/shared/i_loader.dart';
import 'package:base_project_template/services/dialog_service/dialogs/loader_dialog/loader_widget.dart';
import 'package:base_project_template/store/shared/loader/loader_state.dart';

import 'package:flutter/material.dart';

class LoaderDialog implements ILoader {
  @override
  final bool state;

  @override
  final LoaderKey loaderKey;

  @override
  final String title;

  LoaderDialog({
    required this.state,
    required this.loaderKey,
    required this.title,
  });

  @override
  Widget get widget => LoaderWidget();

  Future<void> _builder(BuildContext context) async {
    return await DialogBuilders.defaultDialogBuilder(
      widget: widget,
      context: context,
    );
  }

  @override
  Future<void> show(DisplayFunction display) async => display(_builder);
}
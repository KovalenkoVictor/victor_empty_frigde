import 'package:flutter/material.dart';
import 'package:base_project_template/services/dialog_service/shared/i_dialog.dart';
import 'package:base_project_template/services/dialog_service/shared/dialog_builders.dart';

import 'notification_error_widget.dart';
class NotificationErrorDialog implements IDialog {
  Future<void> _builder(BuildContext context) {
    return DialogBuilders.defaultDialogBuilder(
      context: context,
      widget: ErrorInternetConnectionWidget(),
    );
  }

  @override
  void show(DisplayFunction display) => display(_builder);
}

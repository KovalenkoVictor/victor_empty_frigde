import 'package:base_project_template/services/dialog_service/dialogs/tutorial_dialog/tutorial_dialog_widget.dart';
import 'package:base_project_template/services/dialog_service/shared/dialog_builders.dart';
import 'package:base_project_template/services/dialog_service/shared/i_dialog.dart';
import 'package:flutter/cupertino.dart';

class TutorialDialog implements IDialog {
  Future<void> _builder(BuildContext context) {
    return DialogBuilders.defaultDialogBuilder(
      context: context,
      widget:TutorialDialogWidget(),
    );
  }

  @override
  void show(DisplayFunction display) => display(_builder);
}

import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/dictionary_classes/pop_up_dictionary.dart';
import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/res/image_assets.dart';
import 'package:base_project_template/ui/layouts/bottom_button/bottom_button.dart';
import 'package:base_project_template/ui/layouts/loader_layout/dialog_layout.dart';
import 'package:flutter/material.dart';

class TutorialDialogWidget extends StatelessWidget {
  PopUpDictionary? languageTex = FlutterDictionary.instance.language?.popUpDictionary ?? en.popUpDictionary;

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return DialogLayout(
      child: Material(
        color: AppColors.kBlack.withOpacity(0.7),
        child: Column(
          children: [
            SizedBox(height: mediaQuery.height * 0.17),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: Text(
                languageTex!.tutorialTitle,
                style: FontStyles.textBold.copyWith(color: AppColors.white, fontSize: 24.0),
                textAlign: TextAlign.center,
              ),
            ),
            const SizedBox(height: 12.0),
            Container(
              height: mediaQuery.height * 0.326,
              margin: EdgeInsets.symmetric(horizontal: 33.0),
              child: Image.asset(ImageAssetsPng.swipeTutorial),
            ),
            const SizedBox(height: 18.0),
            BottomButton(
              onTap: () {
                Navigator.pop(context);
              },
              title: languageTex!.ok,
            ),
          ],
        ),
      ),
    );
  }
}

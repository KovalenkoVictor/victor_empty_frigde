import 'package:base_project_template/services/dialog_service/dialogs/favorites_dialog/favorites_add_dialog/favorites_add_widget.dart';
import 'package:base_project_template/services/dialog_service/shared/dialog_builders.dart';
import 'package:base_project_template/services/dialog_service/shared/i_dialog.dart';
import 'package:flutter/material.dart';

class FavoritesAddDialog implements IDialog {
  Future<void> _builder(BuildContext context) {
    return DialogBuilders.defaultDialogBuilder(
      context: context,
      widget: FavoritesAddWidget(),
    );
  }
  @override
  void show(DisplayFunction display) => display(_builder);
}

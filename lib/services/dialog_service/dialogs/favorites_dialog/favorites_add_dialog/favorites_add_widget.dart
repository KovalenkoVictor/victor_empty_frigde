import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/dictionary_classes/pop_up_dictionary.dart';
import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/const.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/services/dialog_service/dialog_service.dart';
import 'package:base_project_template/ui/layouts/loader_layout/dialog_layout.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable, use_key_in_widget_constructors
class FavoritesAddWidget extends StatelessWidget {
  PopUpDictionary? languageTex = FlutterDictionary.instance.language?.popUpDictionary ?? en.popUpDictionary;

  @override
  Widget build(BuildContext context) {
    Future.delayed(
      SECONDS_2,
          () {
        if (DialogService.instance.isDisplayed) {
          DialogService.instance.close();
        }
      },
    );
    return DialogLayout(
      child: Material(
        color: AppColors.transparent,
        child: InkWell(
          splashColor: AppColors.transparent,
          highlightColor: AppColors.transparent,
          onTap: () {
            DialogService.instance.close();
          },
          child: Center(
            child: Container(
              alignment: Alignment.center,
              width: 248.0,
              height: 52.0,
              decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: BorderRadius.circular(14.0),
                boxShadow: [
                  BoxShadow(
                    blurRadius: 6,
                    spreadRadius: 1,
                    color: AppColors.black6.withOpacity(0.3),
                  ),
                ],
              ),
              child: Text(
                languageTex!.favoritesAdd,
                style: FontStyles.textRegular.copyWith(color: AppColors.black6, fontSize: 16.0),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

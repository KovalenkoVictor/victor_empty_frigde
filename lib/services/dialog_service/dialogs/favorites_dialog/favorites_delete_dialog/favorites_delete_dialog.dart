import 'package:base_project_template/services/dialog_service/dialogs/favorites_dialog/favorites_delete_dialog/favorites_delete_widget.dart';
import 'package:base_project_template/services/dialog_service/shared/dialog_builders.dart';
import 'package:base_project_template/services/dialog_service/shared/i_dialog.dart';
import 'package:flutter/material.dart';

class FavoritesDeleteDialog implements IDialog {
  Future<void> _builder(BuildContext context) {
    return DialogBuilders.defaultDialogBuilder(
      context: context,
      widget: FavoritesDeleteWidget(),
    );
  }

  @override
  void show(DisplayFunction display) => display(_builder);
}

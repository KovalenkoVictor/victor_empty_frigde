import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/services/dialog_service/dialog_service.dart';
import 'package:base_project_template/services/dialog_service/dialogs/forgot_your_password/forgot_your_password_dialog_widget/widget_enter_your_email.dart';
import 'package:base_project_template/ui/layouts/loader_layout/dialog_layout.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

// ignore: use_key_in_widget_constructors
class ForgotYourPasswordDialogWidget extends StatefulWidget {
  @override
  _ForgotYourPasswordDialogWidgetState createState() => _ForgotYourPasswordDialogWidgetState();
}

class _ForgotYourPasswordDialogWidgetState extends State<ForgotYourPasswordDialogWidget> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _codeController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController = TextEditingController();
  final CarouselController _carouselController = CarouselController();

  ScrollPhysics scrollPhysics = ScrollPhysics();
  int numberDialog = 0;

  @override
  void dispose() {
    _emailController.dispose();
    _codeController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (numberDialog > 0) {
          setState(() {
            numberDialog--;
            _carouselController.animateToPage(numberDialog, duration: Duration(milliseconds: 500), curve: Curves.fastOutSlowIn);
          });
          return false;
        } else {
          return true;
        }
      },
      child: GestureDetector(
        onTapDown: (details) => FocusScope.of(context).requestFocus(FocusNode()),
        child: DialogLayout(
          child: Material(
            color: AppColors.kBlack.withOpacity(0.7),
            child: Stack(
              children: [
                SizedBox(
                  width: double.infinity,
                  height: double.infinity,
                  child: InkWell(
                    splashColor: AppColors.transparent,
                    highlightColor: AppColors.transparent,
                    onTap: () {
                      DialogService.instance.close();
                    },
                  ),
                ),
                Column(
                  children: [
                    const SizedBox(
                      height: 204.0,
                    ),
                    CarouselSlider(
                      carouselController: _carouselController,
                      options: CarouselOptions(
                        viewportFraction: 1,
                        scrollPhysics: NeverScrollableScrollPhysics(),
                        enableInfiniteScroll: false,
                        height:  320,
                        initialPage: numberDialog,
                      ),
                      items: [
                        WidgetEnterYourEmail(
                          lastDialog: false,
                          hintText: 'Email',
                          buttonText: 'Send',
                          height: 257.0,
                          title: 'Enter your email',
                          callBack: () {
                            setState(() {
                              if (numberDialog < 3) {
                                numberDialog++;
                                _carouselController.animateToPage(numberDialog, duration: Duration(milliseconds: 500), curve: Curves.fastOutSlowIn);
                              }
                            });
                          },
                          textEditingController: _emailController,
                        ),
                        WidgetEnterYourEmail(
                          lastDialog: false,
                          hintText: 'Code',
                          buttonText: 'Send',
                          height: 283.0,
                          title: 'We have sent a verification code to your email.Enter a code',
                          callBack: () {
                            setState(() {
                              if (numberDialog < 3) {
                                numberDialog++;
                                _carouselController.animateToPage(numberDialog, duration: Duration(milliseconds: 500), curve: Curves.fastOutSlowIn);
                              }
                            });
                          },
                          textEditingController: _codeController,
                        ),
                        WidgetEnterYourEmail(
                          hintTwoText: 'Confirm Password',
                          lastDialog: true,
                          hintText: 'Password',
                          buttonText: 'Save',
                          height: 320.0,
                          title: 'Enter a new password',
                          callBack: () {
                            DialogService.instance.close();
                          },
                          textEditingController: _passwordController,
                          textEditingControllerTwo: _confirmPasswordController,
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

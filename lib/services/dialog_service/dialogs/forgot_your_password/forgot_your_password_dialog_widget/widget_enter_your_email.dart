import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/services/dialog_service/dialog_service.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/sign_in_sign_up_state/sign_in_sign_up_vm.dart';
import 'package:base_project_template/ui/layouts/bottom_button/bottom_button.dart';
import 'package:base_project_template/ui/layouts/custom_text_field/custom_text_field.dart';
import 'package:base_project_template/ui/pages/sign_in_sign_up/sign_in_sign_up_show_button_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

// ignore: use_key_in_widget_constructors
class WidgetEnterYourEmail extends StatelessWidget {
  final bool lastDialog;
  final String title;
  final String hintText;
  final String? hintTwoText;
  final String buttonText;
  final double height;

  final TextEditingController textEditingController;
  final TextEditingController? textEditingControllerTwo;
  final Function() callBack;

  // ignore: use_key_in_widget_constructors
  WidgetEnterYourEmail({
    required this.textEditingController,
    required this.callBack,
    required this.title,
    required this.height,
    required this.hintText,
    required this.buttonText,
    required this.lastDialog,
     this.textEditingControllerTwo,
    this.hintTwoText,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: StoreConnector<AppState, SignInSignUpViewModel>(
        converter: SignInSignUpViewModel.fromStore,
        builder: (context, vm) => Container(
          margin: EdgeInsets.symmetric(horizontal: 15.0),
          height: height,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12.0),
            color: AppColors.white,
            boxShadow: [
              BoxShadow(
                color: AppColors.blackTwo.withOpacity(0.25),
                spreadRadius: 2,
                blurRadius: 5,
              ),
            ],
          ),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 6.0, right: 6.0),
                width: double.infinity,
                alignment: Alignment.centerRight,
                child: Material(
                  color: AppColors.transparent,
                  child: SizedBox(
                    width: 40.0,
                    height: 40.0,
                    child: InkWell(
                      borderRadius: BorderRadius.circular(360.0),
                      onTap: () {
                        DialogService.instance.close();
                      },
                      child: Icon(
                        Icons.close,
                        color: AppColors.pastelRed,
                      ),
                    ),
                  ),
                ),
              ),
              Row(
                children: [
                  Spacer(),
                  SizedBox(
                    width: 275.0,
                    child: Text(
                      title,
                      style: FontStyles.textRegular.copyWith(
                        color: AppColors.blackTwo,
                        fontSize: 16.0,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Spacer(),
                ],
              ),
              const SizedBox(height: 19.0),
              !lastDialog
                  ? CustomTextField(
                      margin: EdgeInsets.only(left: 9.0, right: 9.0),
                      obscureText: false,
                      color: AppColors.pinkishGrey,
                      textFieldLoaderAndShow: false,
                      controller: textEditingController,
                      hintTextField: hintText,
                      iconSearch: false,
                      textFieldInContainer: true,
                    )
                  : Column(
                      children: [
                        CustomTextField(
                          margin: EdgeInsets.only(left: 9.0, right: 9.0),
                          obscureText: vm.needThreeShowValue,
                          color: AppColors.pinkishGrey,
                          textFieldLoaderAndShow: true,
                          controller: textEditingController,
                          hintTextField: hintText,
                          iconSearch: false,
                          textFieldInContainer: true,
                          endTextField: SignInSingUpShowButtonText(
                            numberShow: 3,
                          ),
                        ),
                        const SizedBox(height: 16.0),
                        CustomTextField(
                          margin: EdgeInsets.only(left: 9.0, right: 9.0),
                          obscureText: vm.needFourShowValue,
                          color: AppColors.pinkishGrey,
                          textFieldLoaderAndShow: true,
                          controller: textEditingControllerTwo,
                          hintTextField: hintTwoText,
                          iconSearch: false,
                          textFieldInContainer: true,
                          endTextField: SignInSingUpShowButtonText(
                            numberShow: 4,
                          ),
                        )
                      ],
                    ),
              const SizedBox(height: 30.0),
              BottomButton(
                margin: EdgeInsets.only(left: 9.0, right: 9.0),
                marginShadow: EdgeInsets.symmetric(horizontal: 22.0),
                title: buttonText,
                onTap: () {
                  callBack();
                },
              ),
              const SizedBox(height: 41.0),
            ],
          ),
        ),
      ),
    );
  }
}

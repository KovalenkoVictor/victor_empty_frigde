import 'package:base_project_template/services/dialog_service/dialogs/forgot_your_password/forgot_your_password_dialog_widget/forgot_your_password_dialog_widget.dart';
import 'package:base_project_template/services/dialog_service/shared/dialog_builders.dart';
import 'package:base_project_template/services/dialog_service/shared/i_dialog.dart';
import 'package:flutter/material.dart';

class ForgotYourPasswordDialog implements IDialog {
  Future<void> _builder(BuildContext context) {
    return DialogBuilders.defaultDialogBuilder(
      context: context,
      widget:ForgotYourPasswordDialogWidget(),
    );
  }
  @override
  void show(DisplayFunction display) => display(_builder);
}
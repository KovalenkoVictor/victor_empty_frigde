import 'package:base_project_template/dictionary/data/en.dart';
import 'package:base_project_template/dictionary/dictionary_classes/pop_up_dictionary.dart';
import 'package:base_project_template/dictionary/flutter_dictionary.dart';
import 'package:base_project_template/res/app_styles/app_colors.dart';
import 'package:base_project_template/res/const.dart';
import 'package:base_project_template/res/fonts.dart';
import 'package:base_project_template/store/application/app_state.dart';
import 'package:base_project_template/store/shared/user_state/user_vm.dart';
import 'package:base_project_template/ui/layouts/bottom_button/bottom_button.dart';
import 'package:base_project_template/ui/layouts/loader_layout/dialog_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

// ignore: must_be_immutable, use_key_in_widget_constructors
class ExitDialogWidget extends StatelessWidget {
  PopUpDictionary? languageTex = FlutterDictionary.instance.language?.popUpDictionary ?? en.popUpDictionary;

  @override
  Widget build(BuildContext context) {
    final Size mediaQuery = MediaQuery.of(context).size;
    return DialogLayout(
      child: Material(
        color: AppColors.kBlack.withOpacity(0.7),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            StoreConnector<AppState, UserViewModel>(
              converter: UserViewModel.fromStore,
              builder: (context, user) => Container(
                padding: EdgeInsets.all(6.0),
                margin: EdgeInsets.only(left: 15.0, right: 15.0, bottom: mediaQuery.height * 0.09),
                height: 273.0,
                decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Column(
                  children: [
                    Container(
                      width: double.infinity,
                      alignment: Alignment.centerRight,
                      child: Material(
                        color: AppColors.transparent,
                        child: SizedBox(
                          width: 40.0,
                          height: 40.0,
                          child: InkWell(
                            borderRadius: BorderRadius.circular(360.0),
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.close,
                              color: AppColors.pastelRed,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: mediaQuery.width * 0.2 - 30),
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          text: languageTex!.logOutTitle,
                          style: FontStyles.textBold.copyWith(fontSize: 24.0, color: AppColors.black6),
                          children: <TextSpan>[
                            TextSpan(
                              text: EMPTY_FRIDGE,
                              style: FontStyles.textBold.copyWith(fontSize: 24.0, color: AppColors.marigold),
                            ),
                            TextSpan(
                              text: '?',
                              style: FontStyles.textBold.copyWith(fontSize: 24.0, color: AppColors.black6),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 34.0),
                    Row(
                      children: [
                        SizedBox(
                          width: mediaQuery.width * 0.48,
                          child: BottomButton(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            title: languageTex!.logOutNo,
                          ),
                        ),
                        Material(
                          color: AppColors.transparent,
                          child: InkWell(
                            borderRadius: BorderRadius.circular(12.0),
                            onTap: () {
                              user.logout();
                            },
                            child: Container(
                              width: mediaQuery.width * 0.32,
                              height: 56,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                border: Border.all(color: AppColors.marigold),
                                borderRadius: BorderRadius.circular(12.0),
                              ),
                              child: Text(
                                languageTex!.logOutYes,
                                style: FontStyles.textMedium.copyWith(color: AppColors.marigold, fontSize: 24.0),
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:base_project_template/services/dialog_service/dialogs/exit_dialog/exit_dialog_widget.dart';
import 'package:flutter/material.dart';
import 'package:base_project_template/services/dialog_service/shared/i_dialog.dart';
import 'package:base_project_template/services/dialog_service/shared/dialog_builders.dart';
class ExitDialog implements IDialog {
  Future<void> _builder(BuildContext context) {
    return DialogBuilders.defaultDialogBuilder(
      context: context,
     widget: ExitDialogWidget(),
    );
  }
  @override
  void show(DisplayFunction display) => display(_builder);
}

import 'package:base_project_template/store/shared/base_action.dart';

class EmptyAction extends BaseAction {
  EmptyAction() : super(type: 'EmptyAction');
}
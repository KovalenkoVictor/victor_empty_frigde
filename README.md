# MyCatalog
MyCatalog it is AppVesto project.

## Architecture - Redux
## Project Links
- [Git](https://bitbucket.org/flutterodessa/base_project_template/src/dev/)
- [Jira](https://appvesto.atlassian.net/secure/RapidBoard.jspa?rapidView=2&projectKey=MC&selectedIssue=MC-23)
- [Design (Not Added)](https://zpl.io/2jnWG4Q)
- [Confluence](https://appvesto.atlassian.net/l/c/BTkHodu2)
- [Jenkins](http://dev.appvesto.com:8080/)

## Flutter Version: 1.20.4
## Dart Version: 2.9.0
## Version History
- 0.0.1 + 1 - No Releases

## Packages
#### Redux
- redux: 3.0.0
- redux_epics: 0.10.6
- flutter_redux: 0.5.3
- flutter_redux_navigation: 0.4.1
- rxdart: 0.22.6

### System
- intl: 0.16.1
- http: 0.12.0+2
- screen: ^0.0.5
- chewie: ^0.9.10
- provider: 4.0.1
- camera: ^0.5.7+3
- flutter_svg: ^0.17.4
- path_provider: ^1.5.1
- qr_code_tools: ^0.0.6
- video_player: ^0.10.11
- cupertino_icons: ^0.1.2
- qr_code_scanner: 0.0.12
- firebase_ml_vision: ^0.9.3+8
- flutter_secure_storage: 3.2.1+1
- flutter_ringtone_player: ^2.0.0
- data_connection_checker: ^0.3.4
- permission_handler: ^4.2.0+hotfix.3
- flutter_keyboard_visibility: ^2.0.0

### Dev pacakges
- build_runner: 1.7.2
- json_serializable: 3.2.5
- flutter_launcher_icons: ^0.7.5

## Additional links
- [Project Code Style](https://appvesto.atlassian.net/l/c/2LAcARuf)
- [Redux Documentation](https://appvesto.atlassian.net/l/c/DMoEFzp2)
- [Jenkins Documentation](https://appvesto.atlassian.net/l/c/1DGmD7k8)
- [Sonar Documentation](https://appvesto.atlassian.net/l/c/5V3nsgTf)